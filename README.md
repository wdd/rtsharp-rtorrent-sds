# RTSharp-rtorrent-SDS & RTSrtorrentRelay (alpha, all releases untested)
## Latest binaries: [Nightly](https://gitlab.com/wdd/rtsharp-rtorrent-sds/builds)

[![build status](https://gitlab.com/wdd/rtsharp-rtorrent-sds/badges/master/build.svg)](https://gitlab.com/wdd/rtsharp-rtorrent-sds/builds)

RTSharp-rtorrent-SDS is a System Data Supplier plugin for RT# which communicates with RTSrtorrentRelay to expose **rtorrent** interface to core application (RT#).
RTSrtorrentRelay is written in [D](https://dlang.org/).

You will need to install RTSharp-rtorrent-SDS on your local machine (bundled with RT# releases) and RTSrtorrentRelay on your remote machine.

**Currently ISFA SSL connections are established without certificate verification. Needs fixing**

![Beautiful connection diagram](https://gitlab.com/wdd/rtsharp-rtorrent-sds/raw/master/beautiful_diagram.png)

## RTSrtorrentRelay
RTSrtorrentRelay is an application which relays rtorrent data to RTSharp-rtorrent-SDS. It is inspired by [ruTorrent](https://github.com/Novik/ruTorrent), which is probably most well-known rtorrent WebUI.

**Q:** Why would you receive rtorrent data, send it to remote machine only to be received once again?

**A:** Because the format in which rtorrent communicates is really truly awful (for example, this is [how you ban a peer](https://i.imgur.com/k5NAvyD.png)), furthermore, if you want to check if anything has changed since last time you checked your torrent list, you must download the entire torrent list (even if nothing changes). With the relay, you only need to receive about 10 bytes of data to know if nothing has changed. The entire torrent list in XML-RPC format is about 500kB (with 500~ torrents).


RTSrtorrentRelay runs as an extension to rtorrent and allows more control.

## Building RTSrtorrentRelay from source
Clone the RTSrtorrentRelay and install `dub` (the D package manager) and `dmd` (the D compiler) from [APT repository](https://d-apt.sourceforge.net/) or build them from source if you're feeling adventurous.
```
sudo wget http://master.dl.sourceforge.net/project/d-apt/files/d-apt.list -O /etc/apt/sources.list.d/d-apt.list
sudo apt-get update && sudo apt-get -y --allow-unauthenticated install --reinstall d-apt-keyring && sudo apt-get update
sudo apt-get install dmd-bin dub
dub build
```
## Configuring RTSrtorrentRelay
The default configuration file (`config.conf`) **must** be changed. Configure rtorrent port (simply add `scgi_port = localhost:5000` to rtorrent config and then specify `SCGIHost` and `SCGIPort` as `localhost` and `5000`. RTSrtorrentRelay also supports unix sockets (`scgi_local`).), access password (minimum 15 chars, use a secure random password generator), RTSrtorrentRelay listening port and host (don't forget to remove ListenIPV6 setting if you don't need it) and SSL if you want it enabled.
**Change the access password or the server will not start. USE A STRONG PASSWORD. [Compromised access to RTSrtorrentRelay server is a compromised access to your whole server](https://github.com/rakshasa/rtorrent/wiki/COMMAND-Execute).**

## Configuring RT-Sharp-rtorrent-SDS
Upon loading the plugin, a GUI configuration window will appear. Fill in host, port, password, display name (user-friendly name of your server) and you should be all set, provided that RTSrtorrentRelay and rtorrent are configured and running on remote machine.