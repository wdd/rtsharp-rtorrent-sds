﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using RTSharpIFace;
using static RTSharpIFace.Utils;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Net.Security;

namespace RTSharp_rtorrent_SDS {

	public class Main : ISDS {

		internal IRTSharp Host;
		internal List<Conn> Connections = new List<Conn>();
		internal const int BUFFER_SIZE = 4096;
		internal const int TORRENT_HASH_LEN = 20;
		internal Smaz smaz;

		SemaphoreSlim GetTorrentsLock = new SemaphoreSlim(1);
		
		public class Conn
		{
			public SemaphoreSlim Lock;
			public TcpClient Client;
			public Stream Stream;
			public uint AllTorrentRowsTag;
#if DEBUG
			public ulong LockTimestamp;
			public int ConnectionID;
#endif
		}

		[Serializable]
		internal struct FILE_ID
		{
			internal int id;
			internal string fullPath;

			public FILE_ID(int id, string fullPath) {
				this.id = id;
				this.fullPath = fullPath;
			}
		}

		public enum SCGI_ACTION : byte {
			VERSION = 0,
			GET_ALL_TORRENT_ROWS = 1,
			LOAD_TORRENT_RAW = 2,
			START_TORRENTS_MULTI = 3,
			PAUSE_TORRENTS_MULTI = 4,
			STOP_TORRENTS_MULTI = 5,
			FORCE_RECHECK_TORRENTS_MULTI = 6,
			REANNOUNCE_TORRENTS_MULTI = 7,
			ADD_PEER_TORRENT = 8,
			SET_LABEL_TORRENTS_MULTI = 9,
			SET_PRIORITY_TORRENTS_MULTI = 10,
			REMOVE_TORRENTS_MULTI = 11,
			REMOVE_TORRENTS_PLUS_DATA_MULTI = 12,

			/// <summary>
			/// Gets .torrent files
			/// </summary>
			GET_TORRENT_FILES_MULTI = 13,
			GET_TORRENT_PEERS = 14,

			/// <summary>
			/// Gets list of torrent data files
			/// </summary>
			GET_TORRENT_FILES = 15,
			CUSTOM_CMD = 16,
			GET_SETTINGS = 17,
			SET_SETTINGS = 18,
			TRANSFER_FILE = 19,
			TOGGLE_TRACKERS_MULTI = 20, //
			KICK_PEERS_MULTI = 21, //
			BAN_PEERS_MULTI = 22, //
			TOGGLE_SNUB_PEERS_MULTI = 33, //
			SET_PRIORITY_FILES_MULTI = 34, //
			SET_DOWNLOAD_STRATEGY_FILES_MULTI = 35, //
			MEDIAINFO_FILES_MULTI = 36, //
			DOWNLOAD_FILE = 37, //
			EDIT_TORRENTS_MULTI = 38, //
			LIST_DIRECTORIES = 39,
			CREATE_DIRECTORY = 40,
			I_FR_GET_MTIMES = 41,
			LOAD_TORRENT_STRING = 42,
			GET_TORRENT_TRACKERS = 43,
			SET_TORRENT_DATA_DIR = 44,
			GENERATE_ISFA_TOKEN = 45,
			SEND_ISFA = 46,
			GET_SERVER_STATUS = 47
		}

		// {A47C5CE8-3484-4662-AFF6-1BA700AD894B}
		public Guid GUID => new Guid(new byte[] { 0xa4, 0x7c, 0x5c, 0xe8, 0x34, 0x84, 0x46, 0x62, 0xaf, 0xf6, 0x1b, 0xa7, 0x00, 0xad, 0x89, 0x4b });

		public string Name => "RTSharp-rtorrent-SDS";
		public string Description => "Gets rtorrent data from RTSrtorrentRelay";
		public string Author => "wdd";
		public string Version => Assembly.GetExecutingAssembly().GetName().Version.ToString();

		private Tuple<ulong, ulong> COMPATIBLE_REMOTE_VERSION = Tuple.Create(4294967296UL, 8589934591UL);
		private Regex MagnetNameMatch = new Regex("[A-Fa-f0-9]{40}\\.meta", RegexOptions.Compiled);

		public Guid UniqueGUID { get; private set; }
		public Image OkStatusImage => null;
		public Image NotOkStatusImage => null;
		Image _Logo;
		public Image Logo => _Logo;

		internal string _FriendlyName = "rtorrent";
		public string FriendlyName => _FriendlyName;

		public bool FullUpdate { get; set; }

		internal long Ping;
		internal int Timeout;
		internal bool AutoReconnect;
		internal string HostAddress;
		internal ushort Port;
		internal string AuthPw;
		internal bool SSL;
		internal byte[] SSLHash;
		internal bool SSLHashStored = false;

		internal string IntVersionToString(ulong Version) => (Version >> 40) + "." + ((Version & 0xFF00000000) >> 32) + "." + (Version & 0xFFFFFFFF);

#if DEBUG
		internal Stopwatch desyncTimeout = new Stopwatch();
#endif

		public async Task<Conn> GetAvailableConnection()
		{
			if (Connections.Count == 0) {
				Connect();
				return null;
			}

			for (int x = 0;x < Connections.Count;x++) {
				if (!Connections[x].Client.Connected && AutoReconnect) {
					Host.Log(LOG_LEVEL.INFO, "Connection " + x + " reconnecting...");
					Connect(x);
					continue;
				}
			}

			for (;;) {
				foreach (var c in Connections) {
					if (c.Lock.CurrentCount != 1)
						continue;

					await c.Lock.WaitAsync();
#if DEBUG
					c.LockTimestamp = UnixTimeStamp();
					desyncTimeout.Restart();
#endif

					return c;
				}

#if DEBUG
				if (desyncTimeout.ElapsedMilliseconds > 30000) {
					MessageBox.Show("            WARNING WARNING WARNING\nCONNECTION DE-SYNC AT #" +
						Connections.OrderBy(x => x.LockTimestamp).ToArray()[0].ConnectionID +
						"\nFIRST LOCKED AT " + UnixTimeStampToDateTime(Connections.OrderBy(x => x.LockTimestamp).ToArray()[0].LockTimestamp).ToString("u") +
						"\n\nDETECTED AT " + UnixTimeStampToDateTime(UnixTimeStamp()).ToString("u") + "\n-" +
						UnixTimeStampToDateTime(UnixTimeStamp() - Connections.OrderBy(x => x.LockTimestamp).ToArray()[0].LockTimestamp).ToString("u"), "XXX", MessageBoxButtons.OK, MessageBoxIcon.Stop);
					Debug.Assert(false);
					return null;
				}
#endif
			}
		}

		public void Init(IRTSharp Host, Guid UniqueGUID, WaitingBox WBox) {
			WBox.UpdatePBarSingle(33);
			WBox.UpdateText("RTSharp-rtorrent-SDS: Init");
			this.Host = Host;
			this.UniqueGUID = UniqueGUID;
			smaz = new Smaz();

			WBox.UpdatePBarSingle(66);
			WBox.UpdateText("RTSharp-rtorrent-SDS: Settings...");

			WBox.Hide();

			var ini = Host.GetPluginSettings();
			if (ini.Sections["SDS-Relay"] == null)
				ini.Sections.Add("SDS-Relay");

			var settings = ini.Sections["SDS-Relay"];

			bool newSettings = false;

			if (!settings.Keys.Contains("Timeout") ||
				!settings.Keys.Contains("Connections") ||
				!settings.Keys.Contains("Password") ||
				!settings.Keys.Contains("Port") ||
				!settings.Keys.Contains("Host") ||
				!settings.Keys.Contains("AutoReconnect") ||
				!settings.Keys.Contains("DisplayName") ||
				!settings.Keys.Contains("SSL"))
				newSettings = true;

			Host.SavePluginSettings(ini);

			WBox.UpdatePBarSingle(100);
			WBox.UpdateText("RTSharp-rtorrent-SDS...");

			if (newSettings)
				GetPluginSettings(this).ShowDialog();

			ini = Host.GetPluginSettings();
			settings = ini.Sections["SDS-Relay"];

			WBox.Show();

			if (!settings.Keys.Contains("Timeout") ||
				!settings.Keys.Contains("Connections") ||
				!settings.Keys.Contains("Password") ||
				!settings.Keys.Contains("Port") ||
				!settings.Keys.Contains("Host") ||
				!settings.Keys.Contains("AutoReconnect") ||
				!settings.Keys.Contains("DisplayName") ||
				!settings.Keys.Contains("SSL")) {
				throw new Exception("User failed to configure the plugin");
			}

			Timeout = Int32.Parse(settings.Keys["Timeout"].Value);
			AutoReconnect = Int32.Parse(settings.Keys["AutoReconnect"].Value) == 1;
			_FriendlyName = settings.Keys["DisplayName"].Value;
			SSL = settings.Keys["SSL"].Value == "1";
			if (settings.Keys["SSLHash"] != null)
				SSLHash = Convert.FromBase64String(settings.Keys["SSLHash"].Value);
		}

		public void Unload() {
			foreach (var c in Connections) {
				try {
					c.Stream?.Close();
					c.Client?.Close();
				} catch { /* who craes */ }
			}
		}

		public async Task<dynamic> CustomAccess(dynamic In)
		{
			if (!(In is string))
				return null;

			var s = (string)In;

			var c = await GetAvailableConnection();
			var stm = c.Stream;

			try {
				WriteInt(stm, (byte)SCGI_ACTION.CUSTOM_CMD);
				WriteString(stm, s);

				return await ReadStringAsync(stm);
			} catch (Exception ex) {
				return "EXCEPTION: " + ex;
			} finally {
				c.Lock.Release();
			}
		}

		public ContextMenuStrip GetPluginMenu()
		{
			var cms_main = new ContextMenuStrip();
			var toolStripSeparator1 = new ToolStripSeparator();
			var pingToolStripMenuItem = new ToolStripMenuItem();
			var reconnectToolStripMenuItem = new ToolStripMenuItem();

			cms_main.Items.AddRange(new ToolStripItem[] {
			reconnectToolStripMenuItem,
			toolStripSeparator1,
			pingToolStripMenuItem});
			cms_main.Name = "cms_main";
			cms_main.Size = new System.Drawing.Size(153, 76);
			// 
			// reconnectToolStripMenuItem
			// 
			reconnectToolStripMenuItem.Name = "reconnectToolStripMenuItem";
			reconnectToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			reconnectToolStripMenuItem.Text = "Reconnect";
			// 
			// toolStripSeparator1
			// 
			toolStripSeparator1.Name = "toolStripSeparator1";
			toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
			// 
			// pingToolStripMenuItem
			// 
			pingToolStripMenuItem.Name = "pingToolStripMenuItem";
			pingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			pingToolStripMenuItem.Text = "Ping";

			reconnectToolStripMenuItem.Click += async (Sender, Args) => {
				try {
					FullUpdate = true;
					await Reconnect();
				} catch (Exception ex) {
					Host.Log(LOG_LEVEL.WARN, "Reconnect failed");
					Host.LogException(LOG_LEVEL.WARN, ex);
				}
			};

			pingToolStripMenuItem.Click += async (Sender, Args) => {
				var c = await GetAvailableConnection();
				Stopwatch sw;
				try {
					var stm = c.Stream;
					sw = new Stopwatch();
					sw.Start();
				
					WriteInt(stm, (byte)SCGI_ACTION.VERSION);
					ReadInt<ulong>(stm);
				} catch (Exception ex) {
					Host.Log(LOG_LEVEL.WARN, "Ping failed");
					Host.LogException(LOG_LEVEL.WARN, ex);
					return;
				} finally {
					if (c != null)
						c.Lock.Release();
				}
				sw.Stop();

				Ping = sw.ElapsedMilliseconds;
			};

			return cms_main;
		}

		public Form GetPluginSettings(IPlugin Self)
		{
			return new Settings(Host, Self);
		}

		static internal async Task WriteIntAsync<T>(Stream N, T IIn)
		{
			if (IIn is byte || IIn is sbyte) {
				await N.WriteAsync(new byte[] { (byte)(object)IIn }, 0, 1);
			} else if (IIn is bool)
				await N.WriteAsync(new byte[] { (bool)(object)IIn ? (byte)1 : (byte)0 }, 0, 1);
			else if (IIn is ushort || IIn is short) {
				if (ToType<ushort>(IIn) < 255)
					await N.WriteAsync(new byte[] { ToType<byte>(IIn) }, 0, 1);
				else {
					await N.WriteAsync(new[] { (byte)255 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				}
			} else if (IIn is uint || IIn is int) {
				if (ToType<uint>(IIn) < 254)
					await N.WriteAsync(new byte[] { ToType<byte>(IIn) }, 0, 1);
				else if (ToType<uint>(IIn) < UInt16.MaxValue) {
					await N.WriteAsync(new[] { (byte)254 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				} else {
					await N.WriteAsync(new[] { (byte)255 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<uint>(IIn)), 0, 4);
				}
			} else if (IIn is ulong || IIn is long) {
				if (ToType<ulong>(IIn) < 253)
					await N.WriteAsync(new byte[] { ToType<byte>(IIn) }, 0, 1);
				else if (ToType<ulong>(IIn) < UInt16.MaxValue) {
					await N.WriteAsync(new[] { (byte)253 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				} else if (ToType<ulong>(IIn) < UInt32.MaxValue) {
					await N.WriteAsync(new[] { (byte)254 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<uint>(IIn)), 0, 4);
				} else {
					await N.WriteAsync(new[] { (byte)255 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ulong>(IIn)), 0, 8);
				}
			}
		}

		static internal void WriteInt<T>(Stream N, T IIn)
		{
			if (IIn is byte || IIn is sbyte) {
				N.Write(new byte[] { (byte)(object)IIn }, 0, 1);
			} else if (IIn is bool)
				N.Write(new byte[] { (bool)(object)IIn ? (byte)1 : (byte)0 }, 0, 1);
			else if (IIn is ushort || IIn is short) {
				if (ToType<ushort>(IIn) < 255)
					N.Write(new byte[] { ToType<byte>(IIn) }, 0, 1);
				else {
					N.Write(new[] { (byte)255 }, 0, 1);
					N.Write(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				}
			} else if (IIn is uint || IIn is int) {
				if (ToType<uint>(IIn) < 254)
					N.Write(new byte[] { ToType<byte>(IIn) }, 0, 1);
				else if (ToType<uint>(IIn) < UInt16.MaxValue) {
					N.Write(new[] { (byte)254 }, 0, 1);
					N.Write(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				} else {
					N.Write(new[] { (byte)255 }, 0, 1);
					N.Write(GetBytesNetworkOrder(ToType<uint>(IIn)), 0, 4);
				}
			} else if (IIn is ulong || IIn is long) {
				if (ToType<ulong>(IIn) < 253)
					N.Write(new byte[] { ToType<byte>(IIn) }, 0, 1);
				else if (ToType<ulong>(IIn) < UInt16.MaxValue) {
					N.Write(new[] { (byte)253 }, 0, 1);
					N.Write(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				} else if (ToType<ulong>(IIn) < UInt32.MaxValue) {
					N.Write(new[] { (byte)254 }, 0, 1);
					N.Write(GetBytesNetworkOrder(ToType<uint>(IIn)), 0, 4);
				} else {
					N.Write(new[] { (byte)255 }, 0, 1);
					N.Write(GetBytesNetworkOrder(ToType<ulong>(IIn)), 0, 8);
				}
			}
		}

		static internal T ReadInt<T>(Stream In)
		{
			var first = StmRead(In, 1)[0];

			if (typeof(T) == typeof(byte))
				return ToType<T>(first);
			else if (typeof(T) == typeof(bool))
				return (T)(object)(first == 1);
			else if (typeof(T) == typeof(ushort) || typeof(T) == typeof(short)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(StmRead(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			} else if (typeof(T) == typeof(uint) || typeof(T) == typeof(int)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt32(ArrangeBytesNetworkOrder(StmRead(In, sizeof(uint))), 0));
				else if (first == 254)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(StmRead(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			} else if (typeof(T) == typeof(ulong) || typeof(T) == typeof(long)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt64(ArrangeBytesNetworkOrder(StmRead(In, sizeof(ulong))), 0));
				else if (first == 254)
					return ToType<T>(BitConverter.ToUInt32(ArrangeBytesNetworkOrder(StmRead(In, sizeof(uint))), 0));
				else if (first == 253)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(StmRead(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			}

			throw new SocketException();
		}

		static internal async Task<T> ReadIntAsync<T>(Stream In)
		{
			var first = (await StmReadAsync(In, 1))[0];

			if (typeof(T) == typeof(byte))
				return ToType<T>(first);
			else if (typeof(T) == typeof(bool))
				return (T)(object)(first == 1);
			else if (typeof(T) == typeof(ushort) || typeof(T) == typeof(short)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(await StmReadAsync(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			} else if (typeof(T) == typeof(uint) || typeof(T) == typeof(int)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt32(ArrangeBytesNetworkOrder(await StmReadAsync(In, sizeof(uint))), 0));
				else if (first == 254)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(await StmReadAsync(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			} else if (typeof(T) == typeof(ulong) || typeof(T) == typeof(long)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt64(ArrangeBytesNetworkOrder(await StmReadAsync(In, sizeof(ulong))), 0));
				else if (first == 254)
					return ToType<T>(BitConverter.ToUInt32(ArrangeBytesNetworkOrder(await StmReadAsync(In, sizeof(uint))), 0));
				else if (first == 253)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(await StmReadAsync(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			}

			throw new SocketException();
		}

		static internal void WriteString(Stream In, string SIn)
		{
			var bytes = Encoding.UTF8.GetBytes(SIn);
			WriteInt(In, bytes.Length);
			In.Write(bytes, 0, bytes.Length);
		}

		static internal async Task<string> ReadStringAsync(Stream In)
		{
			var len = await ReadIntAsync<uint>(In);
			if (len == 0) return "";
			return Encoding.UTF8.GetString(await StmReadAsync(In, (int)len, false));
		}

		static internal string ReadString(Stream In)
		{
			var len = ReadInt<uint>(In);
			if (len == 0) return "";
			return Encoding.UTF8.GetString(StmRead(In, (int)len, false));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static internal T ToType<T>(dynamic In)
		{
			//return Convert.ChangeType(In, typeof(T));
			return (T)In;
		}

		static internal byte[] StmRead(Stream In, int Size, bool NetworkReorder = true) {
			byte[] buff = new byte[Size];
			int bytes = 0;
			do {
				bytes += In.Read(buff, bytes, Size - bytes);
				if (bytes == 0)
					throw new IOException("Received 0 bytes, connection lost.");
			} while (bytes != Size);
			return NetworkReorder ? ReorderBytes(buff) : buff;
		}

		static internal async Task<byte[]> StmReadAsync(Stream In, int Size, bool NetworkReorder = true)
		{
			byte[] buff = new byte[Size];
			int bytes = 0;
			do {
				bytes += await In.ReadAsync(buff, bytes, Size - bytes);
				if (bytes == 0)
					throw new IOException("Received 0 bytes, connection lost.");
			} while (bytes != Size);
			return NetworkReorder ? ReorderBytes(buff) : buff;
		}

		static internal byte[] ArrangeBytesNetworkOrder(byte[] In)
		{
			if (BitConverter.IsLittleEndian)
				return In.Reverse().ToArray();
			return In;
		}

		static internal byte[] GetBytesNetworkOrder<T>(T In) {
			var obj = typeof(BitConverter);

			var ret = obj.GetMethod("GetBytes", new[] { typeof(T) }).Invoke(obj, new object[] { In });
			if (BitConverter.IsLittleEndian)
				return ((byte[])ret).Reverse().ToArray();
			return (byte[])ret;
		}

		static internal byte[] ReorderBytes(byte[] In) {
			return BitConverter.IsLittleEndian ? In.Reverse().ToArray() : In;
		}

		public async Task InitConnection()
		{
			await Connect();
		}

		async Task<Stream> ConnectionAuth(TcpClient tcp, string authLogMsg, MadMilkman.Ini.IniFile Settings = null)
		{
			if (!tcp.Connected)
				throw new Exception("Connection failed");

			Stream stm = tcp.GetStream();
			stm.ReadTimeout = Timeout;

			Action<byte[]> storeCertHash = (hash) => {
				MadMilkman.Ini.IniFile ini;
				if (Settings == null)
					ini = Host.GetPluginSettings();
				else
					ini = Settings;
				var settings = ini.Sections["SDS-Relay"].Keys;
				if (settings["SSLHash"] == null)
					settings.Add("SSLHash");
				settings["SSLHash"].Value = Convert.ToBase64String(hash);
				SSLHash = hash;
				Host.SavePluginSettings(ini);
			};

			if (SSL) {
				stm = new SslStream(stm, false, (sender, cert, chain, policy) => {
					if (SSLHash == null) {
						var res = MessageBox.Show(
							"No SSL certificate hash stored.\n" +
							"Remote server certificate hash: " + cert.GetCertHashString() + "\n" +
							"Is this right?",
							"RTSharp-rtorrent-SDS (" + _FriendlyName + " (" + UniqueGUID + "))",
							MessageBoxButtons.YesNo,
							MessageBoxIcon.Question);
						if (res == DialogResult.Yes) {
							storeCertHash(cert.GetCertHash());
							return true;
						}
						return false;
					} else if (!SSLHash.SequenceEqual(cert.GetCertHash())) {
						var res = MessageBox.Show(
							"WARNING!!! Remote certificate has changed:" +
							"\nRemote server certificate hash:\n" + cert.GetCertHashString() + "\n" +
							"Local stored hash:\n" + ByteArrayToHexString(SSLHash) +
							"\nIs this right?",
							"RTSharp-rtorrent-SDS (" + _FriendlyName + " (" + UniqueGUID + "))",
							MessageBoxButtons.YesNo,
							MessageBoxIcon.Error);
						if (res == DialogResult.Yes) {
							storeCertHash(cert.GetCertHash());
							return true;
						}
						return false;
					}

					if (!SSLHashStored) {
						storeCertHash(cert.GetCertHash());
						SSLHashStored = true;
					}

					return true;
				}, null, EncryptionPolicy.RequireEncryption);

				await ((SslStream)stm).AuthenticateAsClientAsync(HostAddress, null, System.Security.Authentication.SslProtocols.Tls12, true);
			}

			Host.Log(LOG_LEVEL.INFO, "Authenticating (" + authLogMsg + ")...");

			stm.Write(Encoding.UTF8.GetBytes(AuthPw), 0, AuthPw.Length);

			var sw = new Stopwatch();
			sw.Start();

			WriteInt(stm, (byte)SCGI_ACTION.VERSION);

			var remoteVersion = ReadInt<ulong>(stm);

			sw.Stop();

			Ping = sw.ElapsedMilliseconds;

			if (remoteVersion < COMPATIBLE_REMOTE_VERSION.Item1 || remoteVersion > COMPATIBLE_REMOTE_VERSION.Item2) {
				throw new Exception("Incompatible relay server version (got v" + IntVersionToString(remoteVersion) +
					", expected between v" + IntVersionToString(COMPATIBLE_REMOTE_VERSION.Item1) + " and v" +
					IntVersionToString(COMPATIBLE_REMOTE_VERSION.Item2));
			}

			return stm;
		}

		async Task Connect(int ConnectionPoolIndex = -1)
		{
			var ini = Host.GetPluginSettings();
			var settings = ini.Sections["SDS-Relay"].Keys;

			string host = settings["Host"].Value;
			if (String.IsNullOrEmpty(host)) {
				if (InputBox.Launch("RT#-Relay", "Please enter RT#-Relay host", false, out host) != DialogResult.OK)
					throw new ConfigException("User failed to configure plugin", "Host");

				settings["Host"].Value = host;
			}
			HostAddress = host;

			string sPort = settings["Port"].Value;
			ushort port;
			if (String.IsNullOrEmpty(sPort)) {
				if (InputBox.Launch("RT#-Relay", "Please enter RT#-Relay port", false, out sPort) != DialogResult.OK)
					throw new ConfigException("User failed to configure plugin", "Port");

				settings["Port"].Value = sPort;
			}

			if (!UInt16.TryParse(sPort, out port))
				throw new ConfigException("User provided invalid value", "Port");
			Port = port;

			AuthPw = settings["Password"].Value;
			if (String.IsNullOrEmpty(AuthPw)) {
				if (InputBox.Launch("RT#-Relay", "Please enter RT#-Relay connection password", true, out AuthPw) != DialogResult.OK)
					throw new ConfigException("User failed to configure plugin", "Password");

				settings["Password"].Value = AuthPw;
			}

			Host.Log(LOG_LEVEL.INFO, "Connecting to " + host + ":" + sPort + "...");

			if (ConnectionPoolIndex == -1) {
				try {
					Connections.ForEach(x => {
						x.Stream.Close();
						x.Client.Close();
					});
				} catch { }
				Connections.Clear();
			}

			var lim = (ConnectionPoolIndex == -1 ? Int32.Parse(settings["Connections"].Value) : ConnectionPoolIndex + 1);

			for (int x = ConnectionPoolIndex == -1 ? 0 : ConnectionPoolIndex;x < lim;x++) {
				var c = new Conn() {
					Lock = new SemaphoreSlim(1)
				};
				
				await c.Lock.WaitAsync();
				try {
					c.Client = new TcpClient();

					c.Client.ReceiveTimeout = Timeout;
					c.Client.SendTimeout = Timeout;

					await c.Client.ConnectAsync(host, port);

					c.Stream = await ConnectionAuth(c.Client, x + " / " + lim, ini);

					/*if (response != 0x01) {
						MessageBox.Show("Invalid password.", "RT#-Relay", MessageBoxButtons.OK);
						settings["Password"].Value = "";
					}*/
				} finally {
					c.Lock.Release();
				}

				if (ConnectionPoolIndex == -1) {
#if DEBUG
					c.ConnectionID = x;
#endif
					Connections.Add(c);
				} else {
					Connections[x] = c;
					break;
				}
			}

			Host.SavePluginSettings(ini);
			FullUpdate = true;
		}

		private async Task<List<TORRENT>> Poll(bool All, bool NewInEvent, List<byte[]> Poll = null) {
			var ret = new List<TORRENT>();
			var newRet = new List<TORRENT>();
			List<byte[]> rmvTorrents;

			var c = await GetAvailableConnection();
			var stm = c.Stream;
			uint removedLen;
			
			try {
				WriteInt(stm, (byte)SCGI_ACTION.GET_ALL_TORRENT_ROWS);

				if (All)
					c.AllTorrentRowsTag = 0;

				stm.Write(GetBytesNetworkOrder(c.AllTorrentRowsTag), 0, sizeof(uint));
				if (Poll != null) {
					stm.Write(new byte[] { 0x01 }, 0, 1);
					WriteInt(stm, (uint)Poll.Count);
					foreach (var t in Poll) {
						Debug.Assert(t.Length == 20);
						stm.Write(t, 0, t.Length);
					}
				} else
					stm.Write(new byte[] { 0x00 }, 0, 1);

				c.AllTorrentRowsTag = BitConverter.ToUInt32(StmRead(stm, sizeof(uint)), 0);

				/*
				 * 
				 */
				Dictionary<int, string> trackerTable = null;
				trackerTable = new Dictionary<int, string>();

				var tableLen = ReadInt<ushort>(stm);
				for (int x = 0; x < tableLen; x++)
					trackerTable.Add(x, ReadString(stm));
				/*
				 * 
				 */

				// Is there actually anything to update?
				uint len = ReadInt<uint>(stm);

				for (int x = 0; x < len; x++) {
					int size;

					TORRENT_STATE state;

					var hash = StmRead(stm, 20, false);
					TORRENT cur = new TORRENT(this, hash);
					size = ReadInt<int>(stm);
					cur.Name = smaz.Decompress(StmRead(stm, size, false));
					state = (TORRENT_STATE)ReadInt<ushort>(stm);
					cur.Size = ReadInt<ulong>(stm);
					cur.Downloaded = ReadInt<ulong>(stm);
					cur.Uploaded = ReadInt<ulong>(stm);
					cur.DLSpeed = ReadInt<uint>(stm);
					cur.UPSpeed = ReadInt<uint>(stm);
					cur.CreatedOnDate = ReadInt<ulong>(stm);
					cur.AddedOnDate = ReadInt<ulong>(stm);
					cur.FinishedOnDate = ReadInt<ulong>(stm);
					cur.Label = ReadString(stm);
					cur.Comment = ReadString(stm);
					cur.RemotePath = ReadString(stm);
					var seedersConnected = ReadInt<uint>(stm);
					var seedersTotal = ReadInt<uint>(stm);
					cur.Seeders = Tuple.Create(seedersConnected, seedersTotal);
					var peersConnected = ReadInt<uint>(stm);
					var peersTotal = ReadInt<uint>(stm);
					cur.Peers = Tuple.Create(peersConnected, peersTotal);
					cur.Priority = (TORRENT_PRIORITY)ReadInt<byte>(stm);
					cur.ChunkSize = ReadInt<uint>(stm);
					cur.Wasted = ReadInt<ulong>(stm);

					var trkLen = ReadInt<ushort>(stm);

					var trackers = new List<TRACKER>();
					for (ushort i = 0;i < trkLen;i++) {
						var curTracker = new TRACKER(cur, i);
						curTracker.Uri = new Uri(trackerTable[ReadInt<ushort>(stm)]);
						curTracker.Status = (TRACKER.TRACKER_STATUS)ReadInt<byte>(stm);
						curTracker.Seeders = ReadInt<uint>(stm);
						curTracker.Peers = ReadInt<uint>(stm);
						curTracker.Downloaded = ReadInt<uint>(stm);
						curTracker.LastUpdated = ReadInt<ulong>(stm);
						curTracker.Interval = ReadInt<uint>(stm);
						var msgId = ReadInt<byte>(stm);
						switch (msgId) {
							case 0:
								curTracker.StatusMsg = "N/A";
								break;
							case 1:
								curTracker.StatusMsg = "Completed";
								break;
							case 2:
								curTracker.StatusMsg = "Started";
								break;
							case 3:
								curTracker.StatusMsg = "Stopped";
								break;
							case 4:
								curTracker.StatusMsg = "Scrape";
								break;

						}
						trackers.Add(curTracker);
					}

					cur.TrackerSingle = trackers
						.Where(t => t.Enabled && t.Uri.OriginalString != "dht://")
						.OrderByDescending(t => t.Seeders)
						.FirstOrDefault()?.Uri;

					if (cur.TrackerSingle == null)
						cur.TrackerSingle = trackers.Count != 0 ? trackers[0].Uri : null;

					cur.Trackers = trackers;

					cur.StatusMsg = ReadString(stm);

					/*
					 * 
					 */
					if (cur.UPSpeed > 1024 || cur.DLSpeed > 1024)
						state |= TORRENT_STATE.ACTIVE;
					else
						state |= TORRENT_STATE.INACTIVE;

					cur.State = state;

					cur.Done = (float)cur.Downloaded / cur.Size * 100;

					cur.RemainingSize = cur.Size - cur.Downloaded;
					cur.Ratio = cur.Downloaded == 0 ? (cur.Uploaded == 0 ? 0 : Single.MaxValue) : (float)cur.Uploaded / cur.Downloaded;
					cur.ETA = cur.DLSpeed == 0 ? 0 : (ulong)(((float)cur.Size - cur.Downloaded) / cur.DLSpeed);

					cur.MagnetDummy = MagnetNameMatch.IsMatch(cur.Name);

					if (!NewInEvent || Host.Torrents.Contains(cur))
						ret.Add(cur);
					else
						newRet.Add(cur);
				}
			
				removedLen = ReadInt<uint>(stm);

				rmvTorrents = new List<byte[]>((int)removedLen);
				for (int x = 0; x < removedLen; x++)
					rmvTorrents.Add(StmRead(stm, 20, false));
			} catch (Exception ex) {
				Host.Log(LOG_LEVEL.ERROR, "Generic error at GetTorrents");
				Host.LogException(LOG_LEVEL.ERROR, ex);
				throw ex;
			} finally {
				c.Lock.Release();
			}

			if (newRet.Count != 0)
				Host.NewTorrents(newRet);
			if (rmvTorrents.Count != 0)
				Host.TorrentsRemoved(rmvTorrents, this);

			return ret;
		}

		public async Task<List<TORRENT>> GetTorrents(IEnumerable<byte[]> Hashes)
		{
			return await Poll(true, false, Hashes?.ToList());
		}

		public async Task<ConnectionStatus> GetStatus()
		{
			await GetTorrentsLock.WaitAsync();

			int activeTorrents = 0;
			ulong globalDown = 0, globalUp = 0;

			foreach (var t in Host.Torrents.Where(x => x.Owner.UniqueGUID.Equals(UniqueGUID))) {
				if (t.State.HasFlag(TORRENT_STATE.ACTIVE))
					activeTorrents++;
				globalDown += t.DLSpeed;
				globalUp += t.UPSpeed;
			}

			var ret = new ConnectionStatus(this, Connections.Count != 0 && Connections.All(x => x.Client.Connected),
				FriendlyName,
				"Ready (" + Connections.Sum(x => x.Client.Connected ? 1 : 0) + " / " + Connections.Count + ") (" + Ping + " ms)\n" +
				"↑ " + GetSizeSI(globalUp) + "/s" + " // ↓ " + GetSizeSI(globalDown) + "/s\n" +
				activeTorrents + " active torrent" + (activeTorrents == 1 ? "" : "s"));
			GetTorrentsLock.Release();
			return ret;
		}

		public SupportedOperations GetSupportedOperations()
		{
			return new SupportedOperations {
				files = {
					DownloadFile = true,
					GetDownloadStrategy = true,
					MediaInfo = true,
					SetDownloadStrategy = true,
					SetPriority = true,
					UploadFile = true,
					GetRemoteDirectories = true,
					CreateRemoteDirectory = true,
					ISFA = true
				},
				torrents = {
					EditTorrent = false,
					GetTorrentFile = true,
					Priority = true,
					FastResume = false,
					ReannounceAll = true,
					Wasted = true,
					SessionDownloadUpload = false,
					StopTorrent = true
				},
				peers = {
					Ban = true,
					Kick = true,
					SnubUnsnub = true
				},
				trackers = {
					EnableDisable = true,
					Reannounce = false,
					SeedersPeersCount = true,
					Update = true
				}
			};
		}

		public async Task SetServerSettings(IEnumerable<ServerSetting> In)
		{
			List<ServerSetting> settings = In as List<ServerSetting>;

			Debug.Assert(settings != null);

			settings = settings.OrderBy(x => x.Id).ToList();

			var c = await GetAvailableConnection();
			var stm = c.Stream;

			var ret = new List<ServerSetting>();

			try {
				WriteInt(stm, (byte)SCGI_ACTION.SET_SETTINGS);

				for (int x = 0;x < settings.Count;x++) {
					if (settings[x].Id == 11 || settings[x].Id == 12)
						settings[x].CurrentValue *= 1024;
						
					if (settings[x].CurrentValue.GetType() == typeof(string))
						WriteString(stm, settings[x].CurrentValue);
					else
						WriteInt(stm, settings[x].CurrentValue);
				}

				// TODO: Handle (+implement) return values
				byte[] buff = new byte[1];
				await stm.ReadAsync(buff, 0, 1);
				Debug.Assert(buff[0] == 1);
				return;
			} finally {
				c.Lock.Release();
			}
		}

		public async Task<IEnumerable<ServerSetting>> GetServerSettings()
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;

			var ret = new List<ServerSetting>();

			try {
				WriteInt(stm, (byte)SCGI_ACTION.GET_SETTINGS);

				string[] curCat = new[] { "Downloads" };

				for (int x = 0; x < 44; x++) {
					var settingId = x;
					var type = GetSettingType((StmRead(stm, sizeof(byte)))[0]);

					dynamic value;
					if (type == typeof(string))
						value = ReadString(stm);
					else
						value = (dynamic)typeof(Main).GetMethod("ReadInt", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).MakeGenericMethod(type).Invoke(null, new[] { stm }); // 👌

					string name = "SETTING ID " + settingId, desc = "N/A";
					dynamic defaultVal = "N/A";
					switch (settingId) {
						/* Downloads */
						case 0:
							name = "Number of upload slots";
							defaultVal = (long)100;
							break;
						case 1:
							name = "Minimum number of peers";
							defaultVal = (long)40;
							break;
						case 2:
							name = "Maximum number of peers";
							defaultVal = (long)100;
							break;
						case 3:
							name = "Minimum number of peers for seeding";
							defaultVal = (long)10;
							break;
						case 4:
							name = "Maximum number of peers for seeding";
							defaultVal = (long)100;
							break;
						case 5:
							name = "Wished number of peers";
							defaultVal = (long)-1;
							break;
						case 6:
							name = "Check hash after download";
							defaultVal = false;
							break;
						case 7:
							name = "Default directory for downloads";
							defaultVal = "/tmp";
							break;

						/* Connection */
						case 8:
							name = "Open listening port";
							defaultVal = true;
							curCat = new[] { "Connection", "Listening" };
							break;
							/* Listening port */
						case 9:
							name = "Port used for incoming connections";
							defaultVal = "10000-15000";
							break;
						case 10:
							name = "Randomize port";
							desc = "Randomize port each time rTorrent starts";
							defaultVal = true;
							break;
							/* Bandwidth Limiting */
						case 11:
							name = "Maximum upload rate";
							desc = "Global maximum upload rate (KiB/s). 0: Unlimited";
							value /= 1024;
							defaultVal = (long)0;
							curCat = new[] { "Connection", "Bandwidth limiting" };
							break;
						case 12:
							name = "Maximum download rate";
							desc = "Global maximum download rate (KiB/s). 0: Unlimited";
							value /= 1024;
							defaultVal = (long)0;
							break;
							/* Other limitations */
						case 13:
							name = "Global number of upload slots";
							defaultVal = (long)0;
							curCat = new[] { "Connection", "Other limitations" };
							break;
						case 14:
							name = "Global number of download slots";
							defaultVal = (long)0;
							break;
						case 15:
							name = "Maximum memory usage (MiB)";
							defaultVal = (long)0;
							break;
						case 16:
							name = "Maximum number of open files";
							defaultVal = (long)128;
							break;
						case 17:
							name = "Maximum number of open http connections";
							defaultVal = (long)32;
							break;

						/* BitTorrent */
						case 18:
							name = "UDP port to use for DHT";
							desc = "Set to 0 to disable";
							defaultVal = (long)6881;
							curCat = new[] { "BitTorrent" };
							break;
						case 19:
							name = "Enable Peer Exchange (PEX)";
							defaultVal = true;
							break;
						case 20:
							name = "IP/Hostname to report to tracker";
							defaultVal = "0.0.0.0";
							break;

						/* Advanced */
						case 21:
							name = "http_cacert";
							defaultVal = "";
							curCat = new[] { "Advanced" };
							break;
						case 22:
							name = "http_capath";
							defaultVal = "";
							break;
						case 23:
							name = "max_downloads_div";
							defaultVal = (long)1;
							break;
						case 24:
							name = "max_uploads_div";
							defaultVal = (long)1;
							break;
						case 25:
							name = "max_file_size";
							defaultVal = (long)137438953472;
							break;
						case 26:
							name = "preload_type";
							defaultVal = new KeyValuePair<string, bool>[3] {
								new KeyValuePair<string, bool>("off", true),
								new KeyValuePair<string, bool>("madvise", false),
								new KeyValuePair<string, bool>("direct paging", false)
							};
							break;
						case 27:
							name = "preload_min_size";
							defaultVal = (long)262144;
							break;
						case 28:
							name = "preload_required_rate";
							defaultVal = (long)5120;
							break;
						case 29:
							name = "receive_buffer_size";
							defaultVal = (long)0;
							break;
						case 30:
							name = "send_buffer_size";
							defaultVal = (long)0;
							break;
						case 31:
							name = "safe_sync";
							defaultVal = false;
							break;
						case 32:
							name = "timeout_safe_sync";
							defaultVal = (long)900;
							break;
						case 33:
							name = "timeout_sync";
							defaultVal = (long)600;
							break;
						case 34:
							name = "scgi_dont_route";
							defaultVal = false;
							break;
						case 35:
							name = "session";
							defaultVal = "/home/user/.config/rtorrent/session/";
							break;
						case 36:
							name = "session_lock";
							defaultVal = true;
							break;
						case 37:
							name = "session_on_completion";
							defaultVal = true;
							break;
						case 38:
							name = "split_file_size";
							defaultVal = (long)-1;
							break;
						case 39:
							name = "split_suffix";
							defaultVal = ".part";
							break;
						case 40:
							name = "use_udp_trackers";
							defaultVal = true;
							break;
						case 41:
							name = "http_proxy";
							defaultVal = "";
							break;
						case 42:
							name = "proxy_address";
							defaultVal = "0.0.0.0";
							break;
						case 43:
							name = "bind";
							defaultVal = "0.0.0.0";
							break;
					}

					ret.Add(new ServerSetting((uint)settingId, name, desc, curCat, defaultVal, value));
				}
			} finally {
				c.Lock.Release();
			}

			return ret;
		}

		Type GetSettingType(byte recv)
		{
			switch (recv) {
				case (int)TypeCode.Boolean:
					return typeof(bool);
				case (int)TypeCode.Byte:
					return typeof(byte);
				case (int)TypeCode.Char:
					return typeof(char);
				case (int)TypeCode.Decimal:
					return typeof(decimal);
				case (int)TypeCode.Double:
					return typeof(double);
				case (int)TypeCode.Int16:
					return typeof(short);
				case (int)TypeCode.Int32:
					return typeof(int);
				case (int)TypeCode.Int64:
					return typeof(long);
				case (int)TypeCode.Single:
					return typeof(float);
				case (int)TypeCode.String:
					return typeof(string);
				case (int)TypeCode.UInt16:
					return typeof(ushort);
				case (int)TypeCode.UInt32:
					return typeof(uint);
				case (int)TypeCode.UInt64:
					return typeof(ulong);
				default:
					throw new ArgumentOutOfRangeException(nameof(recv));
			}
		}

		public async Task Reconnect()
		{
			await Connect();
		}

		public async Task<IEnumerable<TORRENT>> GetAllChanged() {
			return await Poll(false, true);
		}

		private async Task<byte[]> TorrentRawFastResume(byte[] Torrent, string RemotePath)
		{
			var t = new TorrentManip(Torrent);
			var files = new List<string>();
			var libtorrentResume = new BencodeNET.Objects.BDictionary();

			if (t.Torrent.Files != null) {
				foreach (var file in t.Torrent.Files)
					files.Add(PathCombine(RemotePath, file.FullPath));
			} else
				files.Add(PathCombine(RemotePath, t.Torrent.File.FileName));

			libtorrentResume.Add("bitfield", (t.Torrent.TotalSize + t.Torrent.PieceSize - 1) / t.Torrent.PieceSize);

			var mtimes = await ActionEx(SCGI_ACTION.I_FR_GET_MTIMES, null, true, files.Select(x => new[] { x }), async (stm) => {
				var ret = new List<long>();

				for (int x = 0; x < files.Count; x++)
					ret.Add(ReadInt<long>(stm));

				return ret;
			});

			// No files
			if (mtimes.Max() != 0) {
				var resumeFiles = new BencodeNET.Objects.BList();

				for (int x = 0; x < mtimes.Count; x++) {
					var f = new BencodeNET.Objects.BDictionary();

					f.Add("mtime", (long)mtimes[x]);
					f.Add("priority", mtimes[x] != 0 ? 0 : 2);
					f.Add("completed", (long)Math.Ceiling((mtimes.Count == 1 ? t.Torrent.File.FileSize : t.Torrent.Files[x].FileSize) / (double)t.Torrent.PieceSize));
					resumeFiles.Add(f);
				}

				libtorrentResume.Add("files", resumeFiles);
				libtorrentResume.Add("uncertain_pieces.timestamp", (long)UnixTimeStamp());

				t.Torrent.ExtraFields.Add("libtorrent_resume", libtorrentResume);

				var rtorrent = new BencodeNET.Objects.BDictionary();
				rtorrent.Add("chunks_done", libtorrentResume["bitfield"]);
				rtorrent.Add("chunks_wanted", 0);
				rtorrent.Add("complete", 1);
				rtorrent.Add("directory", RemotePath);
				rtorrent.Add("hashing", 0);
				rtorrent.Add("state", 1); // ???
				rtorrent.Add("state_changed", (long)UnixTimeStamp());
				rtorrent.Add("state_counter", 1);
				rtorrent.Add("timestamp.finished", 0);
				rtorrent.Add("timestamp.started", (long)UnixTimeStamp());

				t.Torrent.ExtraFields.Add("rtorrent", rtorrent);

				Torrent = t.Torrent.EncodeAsBytes();
			}

			return Torrent;
		}

		public async Task LoadRaw(byte[] Torrent, bool StartOnAdd, string RemotePath) {
			/*if (FastResume)
				Debug.Assert(RemotePath != null);

			if (FastResume) {
				// Rebuild torrent with libtorrent fastresume data

				var t = new TorrentManip(Torrent);
				var files = new List<string>();
				var libtorrentResume = new BencodeNET.Objects.BDictionary();

				if (t.Torrent.Files != null) {
					foreach (var file in t.Torrent.Files)
						files.Add(PathCombine(RemotePath, file.FullPath));
				} else
					files.Add(PathCombine(RemotePath, t.Torrent.File.FileName));

				libtorrentResume.Add("bitfield", (t.Torrent.TotalSize + t.Torrent.PieceSize - 1) / t.Torrent.PieceSize);

				var mtimes = await TorrentActionEx(SCGI_ACTION.I_FR_GET_MTIMES, null, true, files.Select(x => new[] { x }), async (stm) => {
					var ret = new List<long>();

					for (int x = 0;x < files.Count;x++)
						ret.Add(ReadInt<long>(stm));
					
					return ret;
				});

				// No files
				if (mtimes.Max() != 0) {
					var resumeFiles = new BencodeNET.Objects.BList();

					for (int x = 0;x < mtimes.Count;x++) {
						var f = new BencodeNET.Objects.BDictionary();

						f.Add("mtime", (long)mtimes[x]);
						f.Add("priority", mtimes[x] != 0 ? 0 : 2);
						f.Add("competed", Math.Ceiling((mtimes.Count == 1 ? t.Torrent.File.FileSize : t.Torrent.Files[x].FileSize) / (double)t.Torrent.PieceSize).ToString());
						resumeFiles.Add(f);
					}

					libtorrentResume.Add("files", resumeFiles);

					t.Torrent.ExtraFields.Add("libtorrent_resume", libtorrentResume);
					Torrent = t.Torrent.EncodeAsBytes();
				}
			}*/

			await ActionEx<object>(SCGI_ACTION.LOAD_TORRENT_RAW, new dynamic[] {
				StartOnAdd,
				RemotePath
			}, false, null, async (stm) => {
				WriteInt(stm, Torrent.Length);

				for (int x = 0; x < Math.Ceiling((double)Torrent.Length / BUFFER_SIZE); x++) {
					int blockSize = (Torrent.Length - x * BUFFER_SIZE < BUFFER_SIZE) ? Torrent.Length - x * BUFFER_SIZE : BUFFER_SIZE;
					byte[] block = new byte[blockSize];
					Buffer.BlockCopy(Torrent, x * BUFFER_SIZE, block, 0, blockSize);
					await stm.WriteAsync(block, 0, blockSize);
				}

				return null;
			});
		}

		public async Task LoadMagnet(string Magnet, bool StartOnAdd, string RemotePath) {
			await Action(SCGI_ACTION.LOAD_TORRENT_STRING, new dynamic[] {
				StartOnAdd,
				RemotePath,
				Magnet
			}.AsEnumerable());
		}

		private void StmWriteDynamic<T>(Stream stm, T p)
		{
			if (p is byte[])
				stm.Write(ToType<byte[]>(p), 0, (ToType<byte[]>(p)).Length);
			else if (p is string)
				WriteString(stm, ToType<string>(p));
			else
				WriteInt(stm, p);
		}

		private async Task Action(SCGI_ACTION Action, IEnumerable<dynamic> Params)
		{
			await ActionEx(Action, Params, false, null);
		}

		private async Task TorrentAction(SCGI_ACTION Action, IEnumerable<TORRENT> Torrent)
		{
			await ActionEx(Action, new dynamic[] { }, true, Torrent.Select(x => new dynamic[] { x.Hash }));
		}

		private async Task Action<T>(SCGI_ACTION Action, IEnumerable<T> Arr, Func<T, IEnumerable<dynamic>> Select)
		{
			await ActionEx(Action, null, true, Arr.Select(Select));
		}

		private async Task TorrentAction(SCGI_ACTION Action, IEnumerable<dynamic> ParamsPre, IEnumerable<TORRENT> Torrents)
		{
			await ActionEx(Action, ParamsPre, true, Torrents.Select(x => new dynamic[] { x.Hash }));
		}

		internal async Task<T> ActionEx<T>(
			SCGI_ACTION Action,
			IEnumerable<dynamic> ParamsPre,
			bool SendCount,
			IEnumerable<IEnumerable<dynamic>> ParamsLoop,
			Func<Stream, Task<T>> PostFx)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			try {
				WriteInt(stm, (byte)Action);

				if (ParamsPre != null) {
					foreach (var p in ParamsPre)
						StmWriteDynamic(stm, p);
				}

				if (ParamsLoop != null) {
					if (SendCount)
						WriteInt(stm, ParamsLoop.Count());

					foreach (var x in ParamsLoop) {
						foreach (var p in x)
							StmWriteDynamic(stm, p);
					}
				}

				return await PostFx(stm);
			} finally {
				c.Lock.Release();
			}
		}

		private async Task ActionEx(SCGI_ACTION Action, IEnumerable<dynamic> ParamsPre, bool SendCount, IEnumerable<IEnumerable<dynamic>> ParamsLoop)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			try {
				WriteInt(stm, (byte)Action);

				if (ParamsPre != null) {
					foreach (var p in ParamsPre)
						StmWriteDynamic(stm, p);
				}

				if (ParamsLoop != null) {
					if (SendCount)
						WriteInt(stm, ParamsLoop.Count());

					foreach (var x in ParamsLoop) {
						foreach (var p in x)
							StmWriteDynamic(stm, p);
					}
				}
			} finally {
				c.Lock.Release();
			}
		}

		public async Task Start(IEnumerable<TORRENT> Torrent) {
			await TorrentAction(SCGI_ACTION.START_TORRENTS_MULTI, Torrent);
		}

		public async Task Pause(IEnumerable<TORRENT> Torrent) {
			await TorrentAction(SCGI_ACTION.PAUSE_TORRENTS_MULTI, Torrent);
		}

		public async Task Stop(IEnumerable<TORRENT> Torrent) {
			await TorrentAction(SCGI_ACTION.STOP_TORRENTS_MULTI, Torrent);
		}

		public async Task ForceRecheck(IEnumerable<TORRENT> Torrent) {
			await TorrentAction(SCGI_ACTION.FORCE_RECHECK_TORRENTS_MULTI, Torrent);
		}

		public async Task ReannounceAll(IEnumerable<TORRENT> Torrents) {
			// HACKHACK
			// So it seems that rtorrent doesn't return LastUpdated immediately
			var oldTrackers = new List<List<TRACKER>>();
			foreach (var t in Torrents)
				oldTrackers.Add((await GetTrackers(t)).Item1.ToList());

			await TorrentAction(SCGI_ACTION.REANNOUNCE_TORRENTS_MULTI, Torrents);

			var sw = new Stopwatch();
			List<List<bool>> Updated = new List<List<bool>>();
			for (int x = 0;x < oldTrackers.Count;x++)
				Updated.Add(oldTrackers[x].Select(t => false).ToList());

			do {
				var newTrackers = new List<List<TRACKER>>();
				foreach (var t in Torrents)
					newTrackers.Add((await GetTrackers(t)).Item1.ToList());

				for (int x = 0;x < oldTrackers.Count;x++) {
					for (int y = 0;y < oldTrackers[x].Count;y++) {
						if (oldTrackers[x][y].LastUpdated < 86400) // no hope?
							Updated[x][y] = true;

						if (oldTrackers[x][y].LastUpdated != newTrackers[x][y].LastUpdated)
							Updated[x][y] = true;
					}
				}
				if (!(Updated.All(x => x.All(a => a))))
					await Task.Delay(500);
				else
					break;
			} while (sw.ElapsedMilliseconds < Timeout);
		}

		public async Task AddPeer(TORRENT Torrent, IPAddress PeerIP, ushort PeerPort) {
			await Action(SCGI_ACTION.ADD_PEER_TORRENT, new dynamic[] {
				Torrent.Hash,
				(PeerIP.AddressFamily == AddressFamily.InterNetworkV6 ? "[" + PeerIP + "]" : PeerIP.ToString()) + ":" + PeerPort
			}.AsEnumerable());
		}

		public async Task SetLabel(IEnumerable<TORRENT> Torrent, string Label) {
			await TorrentAction(SCGI_ACTION.SET_LABEL_TORRENTS_MULTI, new dynamic[] { Label }, Torrent);
		}

		public async Task SetPriority(IEnumerable<TORRENT> Torrent, TORRENT_PRIORITY Priority) {
			await TorrentAction(SCGI_ACTION.SET_PRIORITY_TORRENTS_MULTI, new dynamic[] { (byte)Priority }, Torrent);
		}

		public async Task Remove(IEnumerable<TORRENT> Torrent) {
			await TorrentAction(SCGI_ACTION.REMOVE_TORRENTS_MULTI, Torrent);
		}

		public async Task RemoveTorrentAndData(IEnumerable<TORRENT> Torrent) {
			await TorrentAction(SCGI_ACTION.REMOVE_TORRENTS_PLUS_DATA_MULTI, Torrent);
		}

		public async Task<List<byte[]>> GetTorrentFiles(List<TORRENT> Torrent) {
			return await ActionEx(SCGI_ACTION.GET_TORRENT_FILES_MULTI,
				new dynamic[] { },
				true,
				Torrent.Select(x => new dynamic[] { x.Hash }).ToArray(),
				async (stm) => {
					var retCount = await ReadIntAsync<uint>(stm);

					var ret = new byte[retCount][];
					for (int x = 0; x < retCount; x++) {
						var torrentSize = await ReadIntAsync<uint>(stm);
						if (torrentSize == 0)
							ret[x] = null;
						else
								ret[x] = await StmReadAsync(stm, (int)torrentSize, false);
					}

					return ret.ToList();
			});
		}

		public async Task<List<PEER>> GetPeers(TORRENT Torrent) {
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			List<PEER> ret;

			try {
				WriteInt(stm, (byte)SCGI_ACTION.GET_TORRENT_PEERS);
				stm.Write(Torrent.Hash, 0, Torrent.Hash.Length);

				// Are there actually any peers?
				uint len = ReadInt<uint>(stm);

				ret = new List<PEER>();

				for (int x = 0; x < len; x++) {
					var cur = new PEER(Torrent, StmRead(stm, 20, false));

					byte ipv = ReadInt<byte>(stm);
					cur.IP = new IPAddress(StmRead(stm, ipv == 4 ? 4 : 16, false));
					cur.Port = ReadInt<ushort>(stm);
					cur.Client = ReadString(stm);
					cur.Flags = (PEER_FLAGS)(ReadInt<byte>(stm));
					cur.Downloaded = ReadInt<ulong>(stm);
					cur.Uploaded = ReadInt<ulong>(stm);
					cur.DLSpeed = ReadInt<uint>(stm);
					cur.UPSpeed = ReadInt<uint>(stm);
					cur.Done = ReadInt<byte>(stm); // No decimal piece
					ret.Add(cur);
				}
			} finally {
				c.Lock.Release();
			}
			return ret;
		}

		List<FILES> RawFilesTransform(Dictionary<string, FILE> filesRaw, TORRENT Torrent)
		{
			List<FILES> files = new List<FILES>();

			foreach (var el in filesRaw) {
				var dirs = el.Key.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

				FILE curFile = el.Value;
				if (dirs.Length == 1) {
					curFile.Name = dirs[0];
					files.Add(new FILES(-1, curFile, -1));
					files[files.Count - 1].ArrayIndex = files.Count - 1;
					continue;
				}

				Func<int, List<int>, string[], int, int> findParent = null;
				findParent = (root, children, inDirs, startDir) => {
					foreach (int ch in children) {
						if (files[ch].Item.Name == inDirs[startDir] && files[ch].Item.Directory) {
							if (files[ch].Children.Count == 1 && !files[files[ch].Children[0]].Item.Directory)
								return ch;
							return findParent(ch, files[ch].Children, inDirs, startDir + 1);
						}
					}
					return root;
				};

				// Get all root folders
				var indexes = new List<int>();
				for (int x = 0; x < files.Count; x++) {
					if (files[x].Parent == -1 && files[x].Item.Directory) {
						indexes.Add(x);
					}
				}

				var parent = findParent(-1, indexes, dirs, 0);

				curFile.Name = dirs[dirs.Length - 1];

				if (parent == -1 || files[parent].Item.Name != dirs[dirs.Length - 2]) {
					// If we are missing folders

					var xs = 0;
					if (parent != -1) {
						// Find missing folders start
						for (int x = dirs.Length - 1; x >= 0; x--) {
							if (files[parent].Item.Name == dirs[x]) {
								xs = x + 1;
								break;
							}
						}
					}

					for (int x = xs; x < dirs.Length - 1; x++) {
						if (x == 0) {
							// Add folder to root
							var folder = new FILE(dirs[x], Torrent, Torrent.Owner.UniqueGUID, -1) {
								Priority = FILE.PRIORITY.NA,
								DownloadStrategy = FILE.DOWNLOAD_STRATEGY.NA
							};
							files.Add(new FILES(-1, folder, -1));
							files[files.Count - 1].ArrayIndex = files.Count - 1;
						} else {
							// Add other missing folders
							var folder = new FILE(dirs[x], Torrent, Torrent.Owner.UniqueGUID, -1) {
								Priority = FILE.PRIORITY.NA,
								DownloadStrategy = FILE.DOWNLOAD_STRATEGY.NA
							};
							files.Add(new FILES(parent, folder, -1));
							files[files.Count - 1].ArrayIndex = files.Count - 1;
							files[parent].Children.Add(files.Count - 1);
						}
						parent = files.Count - 1;
					}

					// Add the file
					files.Add(new FILES(files.Count - 1, curFile, -1));
					files[files.Count - 1].ArrayIndex = files.Count - 1;
					files[files.Count - 2].Children.Add(files.Count - 1);
				} else {
					// Else, add file at top of existing folders
					files.Add(new FILES(parent, curFile, -1));
					files[files.Count - 1].ArrayIndex = files.Count - 1;
				}
			}

			// Add everything on top of the root folder
			if (files.Count > 1) {
				var folder = new FILE(Torrent.Name, Torrent, Torrent.Owner.UniqueGUID, -1) {
					Priority = FILE.PRIORITY.NA,
					DownloadStrategy = FILE.DOWNLOAD_STRATEGY.NA
				};
				files.Add(new FILES(-1, folder, -1));
				files[files.Count - 1].ArrayIndex = files.Count - 1;
				var i = files.Count - 1;
				for (int x = 0; x < files.Count - 1; x++) {
					if (files[x].Parent == -1) {
						files[x].Parent = i;
						files[i].Children.Add(x);
					}
				}
			}

			return files;
		}

		public async Task<Dictionary<TORRENT, List<FILES>>> GetFiles(List<TORRENT> Torrents)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			var ret = new Dictionary<TORRENT, List<FILES>>();
			var filesRaw = new Dictionary<string, FILE>();

			try {
				WriteInt(stm, (byte)SCGI_ACTION.GET_TORRENT_FILES);
				WriteInt(stm, (uint)Torrents.Count);
				byte[] hashAgg = new byte[Torrents.Count * 20];
				for (int x = 0;x < Torrents.Count;x++)
					Buffer.BlockCopy(Torrents[x].Hash, 0, hashAgg, x * 20, 20);
				
				await stm.WriteAsync(hashAgg, 0, hashAgg.Length);

				for (int x = 0;x < Torrents.Count;x++) {
					var count = ReadInt<uint>(stm);

					for (int i = 0; i < count; i++) {
						var size = ReadInt<ushort>(stm);
						var path = smaz.Decompress(StmRead(stm, size, false));
						var curFile = new FILE(Torrents[x], UniqueGUID, new FILE_ID(i, path));
						curFile.Size = ReadInt<ulong>(stm);
						curFile.DownloadedChunks = ReadInt<ulong>(stm);
						curFile.Priority = (FILE.PRIORITY)ReadInt<byte>(stm);
						curFile.DownloadStrategy = (FILE.DOWNLOAD_STRATEGY)ReadInt<byte>(stm);

						filesRaw.Add(path, curFile);
					}

					ret.Add(Torrents[x], RawFilesTransform(filesRaw, Torrents[x]));
					filesRaw.Clear();
				}
			} finally {
				c.Lock.Release();
			}
			return ret;
		}

		public async Task UploadFile(string LocalFilePath, string RemoteFilePath, IProgress<ulong> ProgressCallback, IProgress<ulong> TotalBytes)
		{
			var tcp = new TcpClient();

			tcp.ReceiveTimeout = Timeout;
			tcp.SendTimeout = Timeout;

			await tcp.ConnectAsync(HostAddress, Port);
			var stm = await ConnectionAuth(tcp, "UploadFile");

			if (File.Exists(LocalFilePath)) {
				RemoteFilePath = RemoteFilePath.Replace('\\', '/');

				var fs = new FileStream(LocalFilePath, FileMode.Open, FileAccess.Read);

				Host.Log(LOG_LEVEL.DEBUG, "Initializing upload from " + LocalFilePath + " to " + RemoteFilePath);

				WriteInt(stm, (byte)SCGI_ACTION.TRANSFER_FILE);
				WriteInt(stm, fs.Length);
				WriteString(stm, RemoteFilePath);

				if (!ReadInt<bool>(stm)) {
					fs.Dispose();
					string err = "Failed to open remote file: " + ReadString(stm);
					throw new IOException(err);
				}

				TotalBytes?.Report((ulong)fs.Length);

				var transferCaps = new Queue<Tuple<TimeSpan, ulong>>();
				var sw = new Stopwatch();
				sw.Start();
				var sSize = GetSizeSI(fs.Length);

				byte[] buff = new byte[4096];
				var blocks = Math.Floor((decimal)fs.Length / 4096);

				Host.Log(LOG_LEVEL.DEBUG, "Uploading from " + LocalFilePath + " to " + RemoteFilePath);

				for (ulong x = 0; x < blocks; x++) {
					await fs.ReadAsync(buff, 0, 4096);
					await stm.WriteAsync(buff, 0, 4096);

					if (x % 50 == 0)
						ProgressCallback?.Report(x * 4096);
				}

				Host.Log(LOG_LEVEL.DEBUG, "Finalizing from " + LocalFilePath + " to " + RemoteFilePath);

				await fs.ReadAsync(buff, 0, (int)(fs.Length % 4096));
				await stm.WriteAsync(buff, 0, (int)(fs.Length % 4096));

				ProgressCallback?.Report((ulong)fs.Length);

				fs.Dispose();

				Host.Log(LOG_LEVEL.INFO, "Upload from " + LocalFilePath + " to " + RemoteFilePath.Replace('\\', '/') + " OK");
			} else {
				Host.Log(LOG_LEVEL.ERROR, "Path " + LocalFilePath);
			}
		}

		public async Task<string> GetDefaultSavePath()
		{
			return (await GetServerSettings()).First(x => x.Name == "Default directory for downloads").CurrentValue;
		}

		public async Task ToggleTracker(IEnumerable<TRACKER> Trackers, bool Enable)
		{
			await ActionEx(
				SCGI_ACTION.TOGGLE_TRACKERS_MULTI,
				new dynamic[] { Enable },
				true,
				Trackers.Select(x => new dynamic[] {
					x.Parent.Hash,
					(ushort)x.ID
				}).ToArray());
		}

		public async Task KickPeer(IEnumerable<PEER> Peers)
		{
			await Action(SCGI_ACTION.KICK_PEERS_MULTI, Peers, x => new dynamic[] { x.Parent.Hash, (byte[])x.ID });
		}

		public async Task BanPeer(IEnumerable<PEER> Peers)
		{
			await Action(SCGI_ACTION.BAN_PEERS_MULTI, Peers, x => new dynamic[] { x.Parent.Hash, (byte[])x.ID });
		}

		public async Task TogglePeerSnub(IEnumerable<PEER> Peers, bool Snub)
		{
			await ActionEx(
				SCGI_ACTION.TOGGLE_SNUB_PEERS_MULTI,
				new dynamic[] { Snub },
				true,
				Peers.Select(x => new dynamic[] {
					x.Parent.Hash,
					(byte[])x.ID
				}).ToArray());
		}

		public async Task SetFilePriority(IEnumerable<FILE> Files, FILE.PRIORITY In)
		{
			await ActionEx(
				SCGI_ACTION.SET_PRIORITY_FILES_MULTI,
				new dynamic[] { (byte)In },
				true,
				Files.Select(x => new dynamic[] {
					x.Parent.Hash,
					((FILE_ID)x.ID).id
				}).ToArray());
		}

		public async Task SetFileDownloadStrategy(IEnumerable<FILE> Files, FILE.DOWNLOAD_STRATEGY In)
		{
			await ActionEx(
				SCGI_ACTION.SET_DOWNLOAD_STRATEGY_FILES_MULTI,
				new dynamic[] { (byte)In },
				true,
				Files.Select(x => new dynamic[] {
					x.Parent.Hash,
					((FILE_ID)x.ID).id
				}).ToArray());
		}

		public async Task FileMediaInfo(IEnumerable<FILE> Files)
		{
			string ret = await ActionEx(SCGI_ACTION.MEDIAINFO_FILES_MULTI, null, true, Files.Select(x => new dynamic[] {
				x.Parent.Hash,
				((FILE_ID)x.ID).id
			}).ToArray(), async (stm) => {
				var ret2 = ReadInt<bool>(stm);
				var str = (ReadString(stm)).Replace("\n", Environment.NewLine);

				if (!ret2)
					throw new Exception("Failed to MediaInfo:" + Environment.NewLine + str);

				return str;
			});

			new ResultsBox(true).Show(ret);
		}

		public async Task DownloadFile(FILE File, string LocalFilePath, IProgress<ulong> ProgressCallback, IProgress<ulong> TotalBytes)
		{
			var tcp = new TcpClient();

			tcp.ReceiveTimeout = Timeout;
			tcp.SendTimeout = Timeout;

			await tcp.ConnectAsync(HostAddress, Port);
			var stm = await ConnectionAuth(tcp, "DownloadFile");

			WriteInt(stm, (byte)SCGI_ACTION.DOWNLOAD_FILE);

			stm.Write(File.Parent.Hash, 0, File.Parent.Hash.Length);
			WriteInt(stm, ((FILE_ID)File.ID).id);

			var status = ReadInt<bool>(stm);

			if (!status)
				throw new Exception("Failed to download file:" + Environment.NewLine + ReadString(stm));

			var fs = new FileStream(LocalFilePath, FileMode.Create);

			var size = ReadInt<ulong>(stm);
			var origSize = size;

			TotalBytes?.Report(size);

			var blocks = Math.Ceiling((decimal)size / 4096);
			var buffer = new byte[4096];
			ulong bytes;
			while (size != 0) {
				bytes = (ulong)(await stm.ReadAsync(buffer, 0, 4096));
				await fs.WriteAsync(buffer, 0, (int)bytes);
				size -= bytes;

				ProgressCallback?.Report(origSize - size);
			}

			fs.Dispose();
		}

		public async Task EditTorrent(IEnumerable<Tuple<TORRENT, IEnumerable<string>>> TorrentNTrackers)
		{
			var tnt = TorrentNTrackers.ToList();
			var torrents = (await GetTorrentFiles(tnt.Select(x => x.Item1).ToList()));

			for (int x = 0;x < tnt.Count;x++) {
				var torrentManip = new TorrentManip(torrents[x]);

				torrentManip.SetTrackers(tnt[x].Item2);
				var raw = torrentManip.Torrent.EncodeAsBytes();

				await Remove(new [] { tnt[x].Item1 });
				await LoadRaw(raw, true, tnt[x].Item1.RemotePath);
			}
		}

		public async Task<IEnumerable<string>> GetRemoteDirectories(string Path)
		{
			return await ActionEx(SCGI_ACTION.LIST_DIRECTORIES, new dynamic[] {
				Path
			}, false, null, async (stm) => {
				List<string> ret = new List<string>();

				var ok = ReadInt<bool>(stm);

				if (!ok)
					throw new Exception(ReadString(stm));

				var count = ReadInt<uint>(stm);
				for (uint x = 0;x < count;x++)
					ret.Add(ReadString(stm));

				return ret.ToArray();
			});
		}

		public async Task CreateRemoteDirectory(string Path)
		{
			Host.Log(LOG_LEVEL.INFO, "Creating remote directory \"" + Path.Replace('\\', '/') + "\"");
			await Action(SCGI_ACTION.CREATE_DIRECTORY, new[] { Path.Replace('\\', '/') });
		}

		public async Task<Tuple<List<TRACKER>, Uri>> GetTrackers(TORRENT Torrent)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			List<TRACKER> ret;

			try {
				WriteInt(stm, (byte)SCGI_ACTION.GET_TORRENT_TRACKERS);
				stm.Write(Torrent.Hash, 0, Torrent.Hash.Length);

				// Are there actually any trackers?
				uint len = ReadInt<uint>(stm);

				ret = new List<TRACKER>();

				for (int x = 0; x < len; x++) {
					var cur = new TRACKER(Torrent, x);

					cur.Uri = new Uri(ReadString(stm));
					cur.Status = (TRACKER.TRACKER_STATUS)ReadInt<byte>(stm);
					cur.Seeders = ReadInt<uint>(stm);
					cur.Peers = ReadInt<uint>(stm);
					cur.Downloaded = ReadInt<uint>(stm);
					cur.LastUpdated = ReadInt<ulong>(stm);
					cur.Interval = ReadInt<uint>(stm);
					ret.Add(cur);
				}
			} finally {
				c.Lock.Release();
			}

			var single = ret
				.Where(t => t.Enabled && t.Uri.OriginalString != "dht://")
				.OrderByDescending(t => t.Seeders)
				.FirstOrDefault()?.Uri;

			if (single == null)
				single = ret.Count != 0 ? ret[0].Uri : null;

			return Tuple.Create(ret, single);
		}

		public async Task Reannounce(IEnumerable<TRACKER> Trackers)
		{
			throw new NotImplementedException();
		}

		public async Task SetTorrentDataDirectory(IEnumerable<TORRENT> Torrents, string Path)
		{
			List<TORRENT> torrents = Torrents.ToList();

			await Stop(torrents.Where(x => !x.State.HasFlag(TORRENT_STATE.STOPPED)));

			// ISSUE#13
			/*var raws = (await GetTorrentFiles(torrents)).ToList();
			await Remove(torrents);

			for (int x = 0;x < raws.Count;x++) {
				var b = raws[x];
				//var b = await TorrentRawFastResume(raws[x], torrents[x].RemotePath);
				await LoadRaw(b, true, "");
			}*/
			await ActionEx(SCGI_ACTION.SET_TORRENT_DATA_DIR, new dynamic[] { Path.Replace('\\', '/') }, true, Torrents.Select(x => new dynamic[] { x.Hash }), async (stm) => {
				var buff = new byte[1];
				stm.Read(buff, 0, 1);
				return buff[0];
			});
			await Start(Torrents);
		}

		public async Task<ISFA.TOKEN> GetISFAToken(string TargetPath)
		{
			return new ISFA.TOKEN(null, this, await ActionEx(SCGI_ACTION.GENERATE_ISFA_TOKEN, new dynamic[] { TargetPath }, false, null, async (stm) => {
				var status = new byte[1];
				stm.Read(status, 0, 1);
				if (status[0] != 0x01)
					throw new Exception("Remote server returned ISFA token failure");
				
				var token = new byte[32];
				stm.Read(token, 0, 32);
				Debug.WriteLine(token[0]);
				return token;
			}), HostAddress, Port, TargetPath, SSL, SSLHash);
		}

		public async Task SendISFA(ISFA.TOKEN Token, string SourcePath, IProgress<ulong> ProgressCallback, IProgress<ulong> TotalBytes)
		{
			var tcp = new TcpClient();

			tcp.ReceiveTimeout = Timeout;
			tcp.SendTimeout = Timeout;

			await tcp.ConnectAsync(HostAddress, Port);
			var stm = await ConnectionAuth(tcp, "ISFA");

			WriteInt(stm, (byte)SCGI_ACTION.SEND_ISFA);
			WriteInt(stm, (uint)Token.Token.Length);
			stm.Write(Token.Token, 0, Token.Token.Length);
			WriteString(stm, Token.Host);
			WriteInt(stm, Token.Port);
			WriteString(stm, SourcePath);
			WriteInt(stm, Token.SSL);
			if (Token.SSL) {
				WriteInt(stm, (ushort)SSLHash.Length);
				stm.Write(Token.SSLHash, 0, SSLHash.Length);
			}

			var status = ReadInt<byte>(stm);
			if (status != 0x01)
				throw new Exception(ReadString(stm));

			var total = ReadInt<ulong>(stm);
			TotalBytes?.Report(total);

			ulong upd = 0;
			do {
				upd = await ReadIntAsync<ulong>(stm);
				ProgressCallback?.Report(upd);
			} while (upd != total);

			tcp.Close();
		}

		public Form GetStatusFull()
		{
			return new Status(this);
		}
	}
}