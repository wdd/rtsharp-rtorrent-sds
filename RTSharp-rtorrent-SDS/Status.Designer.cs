﻿namespace RTSharp_rtorrent_SDS
{
	partial class Status
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.g_disks = new System.Windows.Forms.GroupBox();
			this.lv_disks = new System.Windows.Forms.ListView();
			this.col_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.col_size = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.col_used = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.col_available = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.col_mountPoint = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.l_title = new System.Windows.Forms.Label();
			this.l_cpu = new System.Windows.Forms.Label();
			this.g_throttling = new System.Windows.Forms.GroupBox();
			this.b_reset_up_throttling = new System.Windows.Forms.Button();
			this.b_reset_dl_throttling = new System.Windows.Forms.Button();
			this.l_throttling_up = new System.Windows.Forms.Label();
			this.l_throttling_dl = new System.Windows.Forms.Label();
			this.g_disks.SuspendLayout();
			this.g_throttling.SuspendLayout();
			this.SuspendLayout();
			// 
			// g_disks
			// 
			this.g_disks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.g_disks.Controls.Add(this.lv_disks);
			this.g_disks.Location = new System.Drawing.Point(12, 35);
			this.g_disks.Name = "g_disks";
			this.g_disks.Size = new System.Drawing.Size(358, 176);
			this.g_disks.TabIndex = 0;
			this.g_disks.TabStop = false;
			this.g_disks.Text = "Disks";
			// 
			// lv_disks
			// 
			this.lv_disks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_name,
            this.col_size,
            this.col_used,
            this.col_available,
            this.col_mountPoint});
			this.lv_disks.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lv_disks.GridLines = true;
			this.lv_disks.Location = new System.Drawing.Point(3, 16);
			this.lv_disks.Name = "lv_disks";
			this.lv_disks.Size = new System.Drawing.Size(352, 157);
			this.lv_disks.TabIndex = 0;
			this.lv_disks.UseCompatibleStateImageBehavior = false;
			this.lv_disks.View = System.Windows.Forms.View.Details;
			// 
			// col_name
			// 
			this.col_name.Text = "Name";
			this.col_name.Width = 76;
			// 
			// col_size
			// 
			this.col_size.Text = "Size";
			this.col_size.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.col_size.Width = 61;
			// 
			// col_used
			// 
			this.col_used.Text = "Used";
			this.col_used.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// col_available
			// 
			this.col_available.Text = "Available";
			this.col_available.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// col_mountPoint
			// 
			this.col_mountPoint.Text = "Mount point";
			this.col_mountPoint.Width = 72;
			// 
			// l_title
			// 
			this.l_title.Dock = System.Windows.Forms.DockStyle.Top;
			this.l_title.Location = new System.Drawing.Point(0, 0);
			this.l_title.Name = "l_title";
			this.l_title.Size = new System.Drawing.Size(588, 32);
			this.l_title.TabIndex = 0;
			this.l_title.Text = "Title...";
			this.l_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// l_cpu
			// 
			this.l_cpu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.l_cpu.AutoSize = true;
			this.l_cpu.Location = new System.Drawing.Point(376, 35);
			this.l_cpu.Name = "l_cpu";
			this.l_cpu.Size = new System.Drawing.Size(91, 13);
			this.l_cpu.TabIndex = 1;
			this.l_cpu.Text = "CPU averages: ...";
			// 
			// g_throttling
			// 
			this.g_throttling.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.g_throttling.Controls.Add(this.b_reset_up_throttling);
			this.g_throttling.Controls.Add(this.b_reset_dl_throttling);
			this.g_throttling.Controls.Add(this.l_throttling_up);
			this.g_throttling.Controls.Add(this.l_throttling_dl);
			this.g_throttling.Location = new System.Drawing.Point(379, 51);
			this.g_throttling.Name = "g_throttling";
			this.g_throttling.Size = new System.Drawing.Size(197, 100);
			this.g_throttling.TabIndex = 2;
			this.g_throttling.TabStop = false;
			this.g_throttling.Text = "Throttling";
			// 
			// b_reset_up_throttling
			// 
			this.b_reset_up_throttling.Location = new System.Drawing.Point(147, 40);
			this.b_reset_up_throttling.Name = "b_reset_up_throttling";
			this.b_reset_up_throttling.Size = new System.Drawing.Size(44, 23);
			this.b_reset_up_throttling.TabIndex = 3;
			this.b_reset_up_throttling.Text = "Reset";
			this.b_reset_up_throttling.UseVisualStyleBackColor = true;
			this.b_reset_up_throttling.Click += new System.EventHandler(this.b_reset_up_throttling_Click);
			// 
			// b_reset_dl_throttling
			// 
			this.b_reset_dl_throttling.Location = new System.Drawing.Point(147, 11);
			this.b_reset_dl_throttling.Name = "b_reset_dl_throttling";
			this.b_reset_dl_throttling.Size = new System.Drawing.Size(44, 23);
			this.b_reset_dl_throttling.TabIndex = 2;
			this.b_reset_dl_throttling.Text = "Reset";
			this.b_reset_dl_throttling.UseVisualStyleBackColor = true;
			this.b_reset_dl_throttling.Click += new System.EventHandler(this.b_reset_dl_throttling_Click);
			// 
			// l_throttling_up
			// 
			this.l_throttling_up.AutoSize = true;
			this.l_throttling_up.Location = new System.Drawing.Point(6, 45);
			this.l_throttling_up.Name = "l_throttling_up";
			this.l_throttling_up.Size = new System.Drawing.Size(56, 13);
			this.l_throttling_up.TabIndex = 1;
			this.l_throttling_up.Text = "Upload: ...";
			// 
			// l_throttling_dl
			// 
			this.l_throttling_dl.AutoSize = true;
			this.l_throttling_dl.Location = new System.Drawing.Point(6, 16);
			this.l_throttling_dl.Name = "l_throttling_dl";
			this.l_throttling_dl.Size = new System.Drawing.Size(70, 13);
			this.l_throttling_dl.TabIndex = 0;
			this.l_throttling_dl.Text = "Download: ...";
			// 
			// Status
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(588, 223);
			this.Controls.Add(this.g_throttling);
			this.Controls.Add(this.l_cpu);
			this.Controls.Add(this.l_title);
			this.Controls.Add(this.g_disks);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "Status";
			this.ShowIcon = false;
			this.Text = "Status";
			this.Load += new System.EventHandler(this.Status_Load);
			this.g_disks.ResumeLayout(false);
			this.g_throttling.ResumeLayout(false);
			this.g_throttling.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox g_disks;
		private System.Windows.Forms.Label l_title;
		private System.Windows.Forms.ListView lv_disks;
		private System.Windows.Forms.Label l_cpu;
		private System.Windows.Forms.ColumnHeader col_name;
		private System.Windows.Forms.ColumnHeader col_size;
		private System.Windows.Forms.ColumnHeader col_used;
		private System.Windows.Forms.ColumnHeader col_available;
		private System.Windows.Forms.ColumnHeader col_mountPoint;
		private System.Windows.Forms.GroupBox g_throttling;
		private System.Windows.Forms.Button b_reset_up_throttling;
		private System.Windows.Forms.Button b_reset_dl_throttling;
		private System.Windows.Forms.Label l_throttling_up;
		private System.Windows.Forms.Label l_throttling_dl;
	}
}