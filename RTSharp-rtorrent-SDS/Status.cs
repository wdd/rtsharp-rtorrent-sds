﻿using RTSharpIFace;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTSharp_rtorrent_SDS
{
	public partial class Status : Form
	{
		Main main;
		public Status(Main main)
		{
			this.main = main;
			InitializeComponent();
		}

#pragma warning disable CS4014
		private async void Status_Load(object sender, EventArgs e)
		{

			Task.Run(async () => {
				for (;;) {

					if (!IsHandleCreated) {
						await Task.Delay(100);
						continue;
					}

					object[] res;
					try {
						res = await main.ActionEx(Main.SCGI_ACTION.GET_SERVER_STATUS, null, false, null, async (stm) => {
							var disksCount = Main.ReadInt<ushort>(stm);
							object[] disks = new object[disksCount];
							for (int x = 0;x < disksCount; x++) {
								var name = Main.ReadString(stm);
								var size = Main.ReadInt<ulong>(stm) * 1024;
								var used = Main.ReadInt<ulong>(stm) * 1024;
								var mountPoint = Main.ReadString(stm);
								disks[x] = new object[] { name, size, used, mountPoint };
							}

							var cpu1pc = Main.ReadInt<uint>(stm);
							var cpu5pc = Main.ReadInt<uint>(stm);
							var cpu15pc = Main.ReadInt<uint>(stm);

							float cpu1, cpu5, cpu15;

							unsafe
							{
								cpu1 = *((float*)&cpu1pc);
								cpu5 = *((float*)&cpu5pc);
								cpu15 = *((float*)&cpu15pc);
							}

							var rtorrentVer = Main.ReadString(stm);
							var libraryVer = Main.ReadString(stm);
							var hostname = Main.ReadString(stm);
						
							return new object[] { disks, cpu1, cpu5, cpu15, rtorrentVer, libraryVer, hostname };
						});
					} catch (Exception ex) {
						main.Host.Log(LOG_LEVEL.ERROR, "Failed to get server status");
						main.Host.LogException(LOG_LEVEL.ERROR, ex);

						Invoke(new MethodInvoker(() => {
							lv_disks.Items.Clear();
							l_cpu.Text = "CPU averages: ??? (fail)";

							l_title.Text = main.FriendlyName + " (" + main.UniqueGUID + ") fail";
						}));

						continue;
					}

					Invoke(new MethodInvoker(() => {
						lv_disks.Items.Clear();
						foreach (object[] d in (object[])res[0]) {
							lv_disks.Items.Add(new ListViewItem(new [] {
								(string)d[0],
								Utils.GetSizeSI((ulong)d[1]),
								Utils.GetSizeSI((ulong)d[2]),
								Utils.GetSizeSI((ulong)d[1] - (ulong)d[2]),
								(string)d[3]
							}));
						}
						
						l_cpu.Text = "CPU averages: " + Math.Round((float)res[1], 2) + ", " + Math.Round((float)res[2], 2) + ", " + Math.Round((float)res[3], 2);

						l_title.Text = main.FriendlyName + " (" + main.UniqueGUID + ") @ " + (string)res[6] + " hosting rtorrent " + (string)res[4] + " / " + (string)res[5];
					}));

					await Task.Delay(RTSharpIFace.Settings.RefreshInterval);
				}
			});


			try {
				var settings = await main.GetServerSettings();
				var dlThrottling = settings.First(x => x.Name == "Maximum download rate");
				var upThrottling = settings.First(x => x.Name == "Maximum upload rate");

				if (dlThrottling.CurrentValue == 0)
					l_throttling_dl.Text = "Download: Unlimited";
				else
					l_throttling_dl.Text = "Download: " + Utils.GetSizeSI(dlThrottling.CurrentValue * 1024) + "/s";
				
				if (upThrottling.CurrentValue == 0)
					l_throttling_up.Text = "Upload: Unlimited";
				else
					l_throttling_up.Text = "Upload: " + Utils.GetSizeSI(upThrottling.CurrentValue * 1024) + "/s";
			} catch (Exception ex) {
				main.Host.Log(LOG_LEVEL.ERROR, "Failed to get server settings");
				main.Host.LogException(LOG_LEVEL.ERROR, ex);
				l_throttling_dl.Text = "Download: ??? (fail)";
				l_throttling_up.Text = "Upload: ??? (fail)";
			}
		}
#pragma warning restore CS4014

		async Task<bool> ResetSetting(string Name)
		{
			try {
				var settings = await main.GetServerSettings();

				foreach (var c in settings.Where(x => x.Name == Name))
					c.CurrentValue = 0;

				await main.SetServerSettings(settings);

				return true;
			} catch (Exception ex) {
				main.Host.Log(LOG_LEVEL.ERROR, "Failed to get/set server settings");
				main.Host.LogException(LOG_LEVEL.ERROR, ex);
				MessageBox.Show("Failed to reset setting", "RTSharp-rtorrent-SDS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

				return false;
			}
		}

		private async void b_reset_dl_throttling_Click(object sender, EventArgs e)
		{
			if (await ResetSetting("Maximum download rate"))
				l_throttling_dl.Text = "Download: Unlimited";
		}

		private async void b_reset_up_throttling_Click(object sender, EventArgs e)
		{
			if (await ResetSetting("Maximum upload rate"))
				l_throttling_up.Text = "Upload: Unlimited";
		}
	}
}
