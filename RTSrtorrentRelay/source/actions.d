module actions;

import std.variant;
import std.algorithm;
import std.algorithm.iteration;
import std.algorithm.comparison;
import std.file;
import std.socket;
import std.path : expandTilde;
import vibe.d;

import app;
import handle;
import sendEx;
import xmls;
import config;
import xpaths;
import std.xml;
import smaz;

void ActVersion(ref Client client)
{
	ubyte[] toWrite;

	debug logDebug("Version: %d", I_VERSION);

	WriteIntToBuffer(toWrite, I_VERSION);
	client.Stm.write(toWrite);
}

public void SXPre(ref string In) {
	In = In[In.indexOf(">")+1+71 .. $];
}

public void SXPreSingle(ref string In) {
	In = In[In.indexOf(">")+1+31 .. $];
}

public void SXMid(ref string In) pure @nogc @safe {
	In = In[43 .. $];
}

public void SXNestedMultiCallPre(ref string In) pure @nogc @safe {
	In = In[40 .. $];
}

public void SXNestedMultiCallMid(ref string In) pure @nogc @safe {
	In = In[87 .. $];
}

public string SXString(ref string In) pure @nogc @safe {
	auto end = 15;
	while (In[end .. end+2] != "</")
		end++;

	if (In[end-2 .. end] == "/>") {
		In = In[end+8 .. $];
		return "";
	}
	scope (exit) In = In[end+17 .. $];
	return In[15 .. end];
}

public long SXInteger(ref string In) pure @safe {
	auto end = 11;
	while (In[end] != '/') while (In[end++] != '<') { }
	end--;
	scope (exit) In = In[end+13 .. $];
	return to!long(In[11 .. end]);
}

public bool SXBool(ref string In) pure @safe {
	scope (exit) In = In[25 .. $];
	return In[11] != '0';
}

public SXType SXGetType(ref string In) pure @nogc @safe
{
	if (In[9] == '8')
		return SXType.I8;
	else if (In[9] == '4')
		return SXType.I4;
	else
		return SXType.String;
}

public enum SXType {
	NestedMultiCallMid = 87,
	Mid = 43,
	NestedMultiCallPre = 40,
	Bool = 25,

	I4 = 1,
	I8 = 2,
	String = 3
}

public template SXSkip(alias In, int Type)
{
	enum SXSkip = In.stringof ~ ` = ` ~ In.stringof ~ `[` ~ to!string(Type) ~ ` .. $];`;
}

public template SXToEnd(alias In)
{
	enum SXToEnd = In.stringof ~ `[0 .. 3] != "ue>"`;
}

void ActGetTorrents(ref Client client, Variant[] Data, string ResXml)
{
	TORRENT curTorrent;
	ubyte[] toWrite, writeTracker;
	string stmp;

	auto sendOnlyUpdate = Data[0].get!(bool);
	ubyte[][] hashes;
	if (Data.length == 2) {
		hashes = Data[1].get!(ubyte[][]);
		logInfo("got hashes");
	}
		
	auto stm = client.Stm;

	if (!sendOnlyUpdate)
		client.LastTorrentList.clear();

	TORRENT[ubyte[20]] curTorrentList;
	ushort[string] trackerTable;
	ushort tableInc = 0;

	SXPre(ResXml);
	
	while (mixin(SXToEnd!(ResXml))) {
		auto name = decode(SXString(ResXml));
		curTorrent.Name = cSmaz.Compress(name);
		curTorrent.Size = SXInteger(ResXml);
		curTorrent.Downloaded = SXInteger(ResXml);
		curTorrent.Uploaded = SXInteger(ResXml);
		curTorrent.DLSpeed = cast(uint)SXInteger(ResXml);
		curTorrent.UPSpeed = cast(uint)SXInteger(ResXml);
		curTorrent.CreatedOn = SXInteger(ResXml);
		stmp = SXString(ResXml);
		curTorrent.AddedOn = stmp == "" ? 0UL : to!ulong(stmp);
		/+stmp = SXString(ResXml);
		curTorrent.FinishedOn = stmp == "" ? 0UL : to!ulong(stmp);+/
		curTorrent.FinishedOn = SXInteger(ResXml);
		curTorrent.Label = decode(SXString(ResXml));
		curTorrent.Comment = decode(SXString(ResXml));
		
		if (curTorrent.Comment.length >= 10 && curTorrent.Comment[0 .. 10] == "VRS24mrker")
			curTorrent.Comment = curTorrent.Comment[10 .. $];
		
		curTorrent.Path = expandTilde(decode(SXString(ResXml)));
		curTorrent.Priority = cast(TORRENT_PRIORITY)SXInteger(ResXml);
		curTorrent.ChunkSize = cast(uint)SXInteger(ResXml);
		curTorrent.WastedBytes = SXInteger(ResXml);

		auto trackers = split(SXString(ResXml), "*");
		curTorrent.Trackers.length = 0;

		foreach (tr; trackers) {
			if (tr == "")
				continue;

			auto trk = new TRACKER;
			curTorrent.Trackers ~= trk;
			TRACKER_STATUS status;

			auto parts = split(tr, "@");
			auto uri = decode(parts[0]);
			status |= parts[1] == "1" ? TRACKER_STATUS.ENABLED : TRACKER_STATUS.DISABLED;
			status |= parts[2] == "1" ? TRACKER_STATUS.ACTIVE : TRACKER_STATUS.NOT_ACTIVE;
			trk.Status = status;
			trk.Seeders = to!uint(parts[3]);
			trk.Peers = to!uint(parts[4]);
			trk.Downloaded = to!uint(parts[5]);
			trk.LastUpdated = to!ulong(parts[6]);
			trk.Interval = to!uint(parts[7]);
			trk.StatusMsg = to!ubyte(parts[8]);
			
			auto pTable = uri in trackerTable;
			
			if (pTable == null) {
				trackerTable[uri] = tableInc;

				WriteStringToBuffer(toWrite, uri);
				trk.Uri.UriCode = tableInc;
				tableInc++;
			} else
				trk.Uri.UriCode = *pTable;
		}
		
		curTorrent.StatusMsg = SXString(ResXml);

		auto open = SXBool(ResXml);
		auto state = SXBool(ResXml);
		auto hashing = SXInteger(ResXml);
		auto rechecking = hashing != 0;

		if (!open)
			curTorrent.State = curTorrent.Size == curTorrent.Downloaded ? TORRENT_STATE.COMPLETE : TORRENT_STATE.STOPPED;
		else {
			if (rechecking)
				curTorrent.State = TORRENT_STATE.HASHING;
			else {
				if (state)
					curTorrent.State = (curTorrent.Size == curTorrent.Downloaded) ? TORRENT_STATE.SEEDING : TORRENT_STATE.DOWNLOADING;
				else
					curTorrent.State = TORRENT_STATE.PAUSED;
			}
		}
		
		if (curTorrent.StatusMsg.length != 0 && curTorrent.StatusMsg != "Tracker: [Tried all trackers.]")
			curTorrent.State |= TORRENT_STATE.ERRORED;

		if (SXBool(ResXml))
			curTorrent.State |= TORRENT_STATE.PRIVATE;

		curTorrent.SeedersConnected = cast(uint)SXInteger(ResXml);
		curTorrent.SeedersTotal = to!uint(sum(map!(el => el == "" ? 0 : to!uint(el))(split(SXString(ResXml), "+"))));
		curTorrent.PeersConnected = cast(uint)(SXInteger(ResXml));
		curTorrent.PeersTotal = to!uint(sum(map!(el => el == "" ? 0 : to!uint(el))(split(SXString(ResXml), "+"))));
		ubyte[20] hash;
		auto sHash = SXString(ResXml);
		for (auto a = 0; a < (sHash.length / 2); a++)
			hash[a] = sHash[2*a..2*(a+1)].to!ubyte(16);

		curTorrentList[hash] = curTorrent;

		SXMid(ResXml);
	}

	trackerTable.rehash();
	curTorrentList.rehash();

	WriteIntToBuffer(writeTracker, tableInc);
	stm.write(writeTracker);
	stm.write(toWrite);
	toWrite.length = 0;

	auto toWriteLen = 0;
	foreach(kv; curTorrentList.byKeyValue()) {
		auto t = kv.value;

		auto t2 = kv.key in client.LastTorrentList;

		if (hashes.length != 0) {
			bool found = false;
			foreach (hash; hashes) {
				if (equal(hash, cast(ubyte[])kv.key)) {
					found = true;
					break;
				}
			}
			
			if (!found)
				continue;
		}

		if (t2 !is null &&
			(*t2).UPSpeed == t.UPSpeed &&
			(*t2).PeersConnected == t.PeersConnected &&
			(*t2).PeersTotal == t.PeersTotal &&
			(*t2).SeedersConnected == t.SeedersConnected &&
			(*t2).SeedersTotal == t.SeedersTotal &&
			(*t2).DLSpeed == t.DLSpeed &&
			(*t2).State == t.State &&
			(*t2).Downloaded == t.Downloaded &&
			(*t2).Uploaded == t.Uploaded &&
			(*t2).Priority == t.Priority &&
			(*t2).StatusMsg == t.StatusMsg &&
			(*t2).Trackers.map!(trk => trk.Status).equal(t.Trackers.map!(trk => trk.Status)) &&
			(*t2).Trackers.map!(trk => trk.StatusMsg).equal(t.Trackers.map!(trk => trk.StatusMsg)) &&
			(*t2).Trackers.map!(trk => trk.LastUpdated).equal(t.Trackers.map!(trk => trk.LastUpdated)))
			continue;

		toWrite ~= kv.key; // Hash
		WriteIntToBuffer(toWrite, cast(uint)t.Name.length);
		toWrite ~= cast(ubyte[])t.Name; // Compressed
		WriteIntToBuffer(toWrite, cast(ushort)t.State);
		WriteIntToBuffer(toWrite, t.Size);
		WriteIntToBuffer(toWrite, t.Downloaded);
		WriteIntToBuffer(toWrite, t.Uploaded);
		WriteIntToBuffer(toWrite, t.DLSpeed);
		WriteIntToBuffer(toWrite, t.UPSpeed);
		WriteIntToBuffer(toWrite, t.CreatedOn);
		WriteIntToBuffer(toWrite, t.AddedOn);
		WriteIntToBuffer(toWrite, t.FinishedOn);
		WriteStringToBuffer(toWrite, t.Label); // TODO: Table (like on trackers)
		WriteStringToBuffer(toWrite, t.Comment);
		WriteStringToBuffer(toWrite, t.Path); // TODO: Compress?
		WriteIntToBuffer(toWrite, t.SeedersConnected);
		WriteIntToBuffer(toWrite, t.SeedersTotal);
		WriteIntToBuffer(toWrite, t.PeersConnected);
		WriteIntToBuffer(toWrite, t.PeersTotal);
		WriteIntToBuffer(toWrite, cast(byte)t.Priority);
		WriteIntToBuffer(toWrite, t.ChunkSize);
		WriteIntToBuffer(toWrite, t.WastedBytes);

		WriteIntToBuffer(toWrite, cast(ushort)t.Trackers.length);
		foreach (trk; t.Trackers) {
			WriteIntToBuffer(toWrite, trk.Uri.UriCode);
			WriteIntToBuffer(toWrite, cast(ubyte)trk.Status);
			WriteIntToBuffer(toWrite, trk.Seeders);
			WriteIntToBuffer(toWrite, trk.Peers);
			WriteIntToBuffer(toWrite, trk.Downloaded);
			WriteIntToBuffer(toWrite, trk.LastUpdated);
			WriteIntToBuffer(toWrite, trk.Interval);
			WriteIntToBuffer(toWrite, trk.StatusMsg);
		}

		WriteStringToBuffer(toWrite, t.StatusMsg);
		toWriteLen++;
	}

	ubyte[] tmp;
	WriteIntToBuffer(tmp, toWriteLen);
	stm.write(tmp);
	stm.write(toWrite);
	toWrite.length = 0;
	
	// Removed torrents
	ubyte[] rmvBuff;
	foreach (h; client.LastTorrentList.byKey()) {
		if ((h in curTorrentList) is null) {
			logInfo("rmvBuff + %s", h[0 .. $]);
			rmvBuff ~= h;
		}
	}

	WriteIntToBuffer(toWrite, cast(uint)(rmvBuff.length / 20));
	stm.write(toWrite);
	stm.write(rmvBuff);

	client.LastTorrentList = curTorrentList.dup();
}

void ActLoadRaw(ref Client client, string ResXml)
{
	logInfo("LoadRaw: %s", ResXml);
	// TODO: report of errors
}

void ActStartTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActStopTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActPauseTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActForceRecheckTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActReannounceTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActAddPeerTorrent(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActSetLabelTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActSetPriorityTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActRemoveTorrentsMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActRemoveTorrentsPlusDataMulti(ref Client client, string ResXml)
{
	import std.file : DirEntry, rmdirRecurse;

	SXPre(ResXml);
	while (mixin(SXToEnd!(ResXml))) {
		auto path = SXString(ResXml);
		if (path != "") {
			debug logWarn("rm -rf %s", decode(path));

			DirEntry obj;
			try	obj = DirEntry(expandTilde(decode(path)));
			catch (Exception ex) {
				logWarn("RemoveTorrentsPlusDataMulti DirEntry failed: " ~ ex.msg);
				return;
			}

			if (obj.isDir)
				rmdirRecurse(obj);
			else
				remove(obj.name);
		}

		mixin(SXSkip!(ResXml, SXType.Mid + ((SXType.Bool + SXType.Mid) * 2)));
	}

	// TODO: report of errors
}

void ActGetTorrentFilesMulti(ref Client client, Variant[] Data)
{
	auto stm = client.Stm;
	ubyte[] toWrite;

	auto paths = Data[0].get!(string[]);

	WriteIntToBuffer(toWrite, cast(uint)paths.length);
	stm.write(toWrite);
	toWrite.length = 0;

	foreach (path; paths) {
		ubyte[] file;

		try {
			file = cast(ubyte[])read(expandTilde(path));
		} catch (Exception ex) {
			// TODO: report of errors
		}

		WriteIntToBuffer(toWrite, cast(uint)file.length);
		stm.write(toWrite);
		stm.write(file);
		toWrite.length = 0;
	}
}

void ActGetTorrentPeers(ref Client client, string ResXml)
{
	PEER curPeer;
	PEER[] sendList;
	ubyte[] toWrite;
	auto stm = client.Stm;
	string curIp;

	try {
		SXPre(ResXml);
		while (mixin(SXToEnd!(ResXml))) {
			auto id = SXString(ResXml);
			for (auto x = 0;x < id.length / 2;x++)
				curPeer.ID[x] = id[2*x..2*(x+1)].to!ubyte(16);
	
			curPeer.Client = decode(SXString(ResXml));
			curPeer.Downloaded = SXInteger(ResXml);
			curPeer.Uploaded = SXInteger(ResXml);
			curPeer.DLSpeed = cast(uint)SXInteger(ResXml);
			curPeer.UPSpeed = cast(uint)SXInteger(ResXml);
			curPeer.Done = cast(ubyte)SXInteger(ResXml);
			curPeer.Flags |= SXBool(ResXml) ? PEER_FLAGS.INCOMING : 0;
			curPeer.Flags |= SXBool(ResXml) ? PEER_FLAGS.ENCRYPTED : 0;
			curPeer.Flags |= SXBool(ResXml) ? PEER_FLAGS.SNUBBED : 0;
			curPeer.Flags |= SXBool(ResXml) ? PEER_FLAGS.OBFUSCATED : 0;
	
			curIp = SXString(ResXml);
	
			if (curIp.indexOf(":") == -1)
				curPeer.Addr = new InternetAddress(curIp, cast(ushort)SXInteger(ResXml));
			else
				curPeer.Addr = new Internet6Address(curIp, cast(ushort)SXInteger(ResXml));
	
			sendList ~= curPeer;
			SXMid(ResXml);
		}
	} catch (Exception ex) {
		logWarn("no peers");
		sendList.length = 0;
	}

	if (sendList.length == 0) {
		goto noData;
	} else {
		WriteIntToBuffer(toWrite, cast(uint)sendList.length);
		stm.write(toWrite);
		toWrite.length = 0;
	}

	foreach (p; sendList) {
		toWrite ~= p.ID;
		if (p.Addr.type == typeid(Internet6Address)) {
			WriteIntToBuffer(toWrite, cast(ubyte)6);
			Internet6Address ipv6 = p.Addr.get!(Internet6Address);
			toWrite ~= ipv6.addr();
			WriteIntToBuffer(toWrite, cast(ushort)ipv6.port());
		} else {
			WriteIntToBuffer(toWrite, cast(ubyte)4);
			InternetAddress ipv4 = p.Addr.get!(InternetAddress);
			uint addr = ipv4.addr();
			WriteBinToBuffer(toWrite, addr);
			WriteIntToBuffer(toWrite, cast(ushort)ipv4.port());
		}
		WriteStringToBuffer(toWrite, p.Client);
		WriteIntToBuffer(toWrite, cast(ubyte)p.Flags);
		WriteIntToBuffer(toWrite, p.Downloaded);
		WriteIntToBuffer(toWrite, p.Uploaded);
		WriteIntToBuffer(toWrite, p.DLSpeed);
		WriteIntToBuffer(toWrite, p.UPSpeed);
		WriteIntToBuffer(toWrite, p.Done);

		stm.write(toWrite);
		toWrite.length = 0;
	}

	return;
noData:
	logInfo("no data.");
	WriteIntToBuffer(toWrite, cast(uint)0);
	stm.write(toWrite);
}

void ActGetTorrentFiles(ref Client client, string ResXml, Variant[] Data)
{
	auto appAll = appender!(FILE[][])();
	auto appSingleTorrent = appender!(FILE[])();
	ubyte[] toWrite;
	auto stm = client.Stm;
	auto count = Data[0].get!uint();

	SXPre(ResXml);
	SXNestedMultiCallPre(ResXml);
	while (true) {
		FILE curFile;
		while (mixin(SXToEnd!(ResXml))) {
			curFile.DownloadStrategy = FILE_DOWNLOAD_STRATEGY.NORMAL;

			curFile.Size = SXInteger(ResXml);
			curFile.Priority = cast(FILE_PRIORITY)SXInteger(ResXml); // TODO: always a single byte, SXSingle ?
			curFile.Path = cSmaz.Compress(decode(SXString(ResXml)));
			
			curFile.DownloadedChunks = SXInteger(ResXml);
			
			if (SXBool(ResXml))
				curFile.DownloadStrategy = FILE_DOWNLOAD_STRATEGY.LEADING_CHUNK_FIRST;
			if (SXBool(ResXml))
				curFile.DownloadStrategy = FILE_DOWNLOAD_STRATEGY.TRAILING_CHUCK_FIRST;

			SXMid(ResXml);
			appSingleTorrent.put(curFile);
		}

		appAll.put(appSingleTorrent.data.dup());
		appSingleTorrent.clear();

		if (ResXml.length >= SXType.NestedMultiCallMid) {
			SXNestedMultiCallMid(ResXml);
		} else
			break;
	}

	foreach (files; appAll.data) {
		WriteIntToBuffer(toWrite, cast(uint)files.length);
		foreach (f; files) {
			WriteStringToBuffer(toWrite, f.Path);
			WriteIntToBuffer(toWrite, f.Size);
			WriteIntToBuffer(toWrite, f.DownloadedChunks);
			WriteIntToBuffer(toWrite, cast(ubyte)f.Priority);
			WriteIntToBuffer(toWrite, cast(ubyte)f.DownloadStrategy);
		}
	}

	stm.write(toWrite);
	toWrite.length = 0;
}

void ActExecCustomCommand(ref Client client, string ResXml)
{
	auto stm = client.Stm;
	ubyte[] toWrite;

	logInfo("XML len %d", ResXml.length);

	WriteStringToBuffer(toWrite, ResXml);
	logInfo("XML %s", ResXml);
	stm.write(toWrite);
}

void ActGetSettings(ref Client client, string ResXml)
{
	auto stm = client.Stm;
	auto boolSettings = [ 6, 8, 10, 19, 31, 34, 36, 37, 40 ];
	auto strSettings = [ 7, 9, 20, 21, 22, 35, 39, 41, 42, 43 ];
	auto x = 0;
	ubyte[] toWrite;

	SXPre(ResXml);
	while (mixin(SXToEnd!(ResXml))) {
		if (strSettings.canFind(x)) {
			toWrite ~= [ cast(ubyte)18 ]; // Data type string
			
			if (x == 7) {
				// Default save path
				WriteStringToBuffer(toWrite, expandTilde(decode(SXString(ResXml))));
			} else
				WriteStringToBuffer(toWrite, decode(SXString(ResXml)));
		} else if (boolSettings.canFind(x))
			toWrite ~= [ cast(ubyte)3 /* Data type bool */, to!ubyte(SXBool(ResXml)) ];
		else {
			toWrite ~= [ cast(ubyte)11 ]; // Data type long
			WriteIntToBuffer(toWrite, SXInteger(ResXml));
		}

		x++;
		SXMid(ResXml);
	}
	
	stm.write(toWrite);
}

void ActSetSettings(ref Client client, string ResXml)
{
	client.Stm.write([ cast(ubyte)1 ]); // All good :-DDD
}

void ActTransferFile(ref Client client)
{
	import std.path;
	import std.file;

	auto stm = client.Stm;
	ubyte[] toWrite;
	auto len = ReadInt!ulong(stm);
	auto dest = expandTilde(ReadString(stm));

	if (exists(dest)) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, "File already exists");
		stm.write(toWrite);
		return;
	}

	FileStream fs;
	
	auto dir = dirName(dest);

	if (!exists(dir))
		createDirectory(dir);
	
	try fs = openFile(dest, FileMode.createTrunc);
	catch (Exception ex) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, ex.msg);
		stm.write(toWrite);
		return;
	}

	WriteIntToBuffer(toWrite, cast(ubyte)1);

	stm.write(toWrite);

	try pipe(stm, fs, len);
	catch (Exception ex) {
		logWarn("Exception at ActTransferFile while writing file: %s", ex.msg);
	}
}

void ActToggleTrackersMulti(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActKickPeer(ref Client client, string ResXml)
{
	logInfo("KICK: %s", ResXml);
	// TODO: report of errors
}

void ActBanPeer(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActToggleSnubPeer(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActSetFilePriority(ref Client client, string ResXml)
{
	logInfo(ResXml);
	// TODO: report of errors
}

void ActSetFileDownloadStrategy(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActFileMediaInfo(ref Client client, string ResXml)
{
	import std.process : execute;
	import std.typecons : Tuple;
	import std.path : buildPath;
	import handle : GetRTXmlResponse;
	import std.algorithm.searching : find;

	auto stm = client.Stm;
	ubyte[] toWrite;

	auto count = ReadInt!uint(stm);
	string[] exec = [ "mediainfo" ];
	
	string ret = "";
	bool ok = true;

	for (auto x = 0;x < count;x++) {
		auto tHash = toHexString(StreamRead(client.Stm, 20));
		auto fIndex = ReadInt!uint(stm);

		auto xml = GetRTXmlResponse("<?xml version=\"1.0\" ?><methodCall><methodName>f.get_frozen_path</methodName><params><param><value><string>" ~ tHash ~ ":f" ~ to!string(fIndex) ~ "</string></value></param></params></methodCall>");
		
		if (xml is null) {
			ret ~= "Failed to connect to rtorrent\n";
			ok = false;
			continue;
		}
		
		SXPreSingle(xml);
		auto path = expandTilde(decode(SXString(xml)));
		if (path.length < 3) {
			ret ~= "No torrent or file.\n";
			ok = false;
			continue;
		}

		exec ~= path;
	}

	if (ok) {
		ok = false;
		Tuple!(int, "status", string, "output") mediainfo;
		try {
			mediainfo = execute(exec);
			if (mediainfo.status == 0) {
				ret = mediainfo.output ~ "\n";
				ok = true;
			} else
				ret ~= "Mediainfo exited with status " ~ to!string(mediainfo.status) ~ "\n";
		} catch (Exception ex) {
			ret = "Error. " ~ ex.msg ~ "\n";
		}
	}

	WriteIntToBuffer(toWrite, ok);
	WriteStringToBuffer(toWrite, ret);
	stm.write(toWrite);
}

void ActDownloadFile(ref Client client, string ResXml)
{
	import std.path : baseName;
	import handle : GetRTXmlResponse;

	auto stm = client.Stm;
	ubyte[] toWrite;

	auto tHash = toHexString(StreamRead(client.Stm, 20));
	auto fIndex = ReadInt!uint(stm);

	auto xml = GetRTXmlResponse("<?xml version=\"1.0\" ?><methodCall><methodName>f.get_frozen_path</methodName><params><param><value><string>" ~ tHash ~ ":f" ~ to!string(fIndex) ~ "</string></value></param></params></methodCall>");
	
	if (xml is null) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, "Failed to connect to rtorrent");
		stm.write(toWrite);
		return;
	}
	
	SXPreSingle(xml);
	auto path = expandTilde(decode(SXString(xml)));
	if (path.length < 3) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, "No torrent or file.");
		stm.write(toWrite);
		return;
	}

	FileStream fs;

	logDebug("open %s", path);

	try fs = openFile(path, FileMode.read);
	catch (Exception ex) {
		logDebug("EX %s", ex.msg);

		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, ex.msg);
		stm.write(toWrite);
		return;
	}

	WriteIntToBuffer(toWrite, cast(ubyte)1);
	WriteIntToBuffer(toWrite, cast(ulong)fs.size);
	logDebug("size %d", fs.size);
	stm.write(toWrite);

	try pipe(fs, stm, fs.size);
	catch (Exception ex) {
		logWarn("Exception at ActDownloadFile while uploading file: %s", ex.msg);
	}
	logDebug("Done.");
}

void ActEditTorrent(ref Client client, string ResXml)
{
	// TODO: report of errors
}

void ActDirectoryListing(ref Client client)
{
	import std.file : dirEntries, DirEntry;

	ubyte[] toWrite;
	DirEntry[] dirs;

	try {
		auto path = expandTilde(ReadString(client.Stm));
		dirs = dirEntries(path, SpanMode.shallow)
			.filter!(x => x.isDir)
			.array();
	} catch (Exception ex) {
		WriteIntToBuffer(toWrite, false);
		WriteStringToBuffer(toWrite, ex.msg);
		client.Stm.write(toWrite);
		return;
	}

	WriteIntToBuffer(toWrite, true);
	WriteIntToBuffer(toWrite, cast(uint)dirs.length);

	foreach (DirEntry dir; dirs)
		WriteStringToBuffer(toWrite, dir.name);
	client.Stm.write(toWrite);
}

void ActCreateDirectory(ref Client client)
{
	import std.file : mkdir;
	import std.path : expandTilde;

	mkdir(expandTilde(ReadString(client.Stm)));

	// OK_HAND
}

void ActIFastResumeGetMTimes(ref Client client)
{
	import std.file : timeLastModified;

	auto stm = client.Stm;
	auto count = ReadInt!uint(stm);
	ubyte[] toWrite;

	for (auto x = 0;x < count;x++) {
		auto path = expandTilde(ReadString(stm));
		SysTime time;

		try	time = timeLastModified(path);
		catch (Exception ex) {
			logWarn("Failed to get modified time on ActIFastResumeGetMTimes, path %s", path);
			WriteIntToBuffer(toWrite, cast(ulong)0);
			continue;
		}

		WriteIntToBuffer(toWrite, time.toUnixTime!long());
	}

	stm.write(toWrite);
}

void ActLoadString(ref Client client, string ResXml)
{
	logInfo("LoadString: %s", ResXml);
	// TODO: report of errors
}

void ActGetTorrentTrackers(ref Client client, string ResXml)
{
	TRACKER curTracker;
	TRACKER[] sendList;
	ubyte[] toWrite;
	auto stm = client.Stm;

	SXPre(ResXml);
	while (mixin(SXToEnd!(ResXml))) {
		TRACKER_STATUS status;
		
		curTracker.Uri.Uri = decode(SXString(ResXml));
		status |= SXBool(ResXml) ? TRACKER_STATUS.ENABLED : TRACKER_STATUS.DISABLED;
		status |= SXBool(ResXml) ? TRACKER_STATUS.ACTIVE : TRACKER_STATUS.NOT_ACTIVE;
		curTracker.Status = status;
		
		curTracker.Seeders = cast(uint)SXInteger(ResXml);
		curTracker.Peers = cast(uint)SXInteger(ResXml);
		curTracker.Downloaded = cast(uint)SXInteger(ResXml);
		curTracker.LastUpdated = SXInteger(ResXml);
		curTracker.Interval = cast(uint)SXInteger(ResXml);
		curTracker.StatusMsg = to!ubyte(SXString(ResXml));

		sendList ~= curTracker;
		SXMid(ResXml);
	}

	if (sendList.length == 0) {
		goto noData;
	} else {
		WriteIntToBuffer(toWrite, cast(uint)sendList.length);
		stm.write(toWrite);
		toWrite.length = 0;
	}

	foreach (t; sendList) {
		WriteStringToBuffer(toWrite, t.Uri.Uri);
		WriteIntToBuffer(toWrite, cast(ubyte)t.Status);
		WriteIntToBuffer(toWrite, t.Seeders);
		WriteIntToBuffer(toWrite, t.Peers);
		WriteIntToBuffer(toWrite, t.Downloaded);
		WriteIntToBuffer(toWrite, t.LastUpdated);
		WriteIntToBuffer(toWrite, t.Interval);
		WriteIntToBuffer(toWrite, t.StatusMsg);

		stm.write(toWrite);
		toWrite.length = 0;
	}

	return;
noData:
	logInfo("no data.");
	WriteIntToBuffer(toWrite, cast(uint)0);
	stm.write(toWrite);
}

void ActSetTorrentDataDir(ref Client client, Variant[] Data, string ResXml)
{
    import utils;
    import std.file : isDir, remove;

	auto targetPath = expandTilde(Data[0].get!string());
	auto hashes = Data[1].get!(ubyte[][])();

	foreach (rawHash; hashes) {
		auto hash = toHexString(rawHash);

		// The "base path" is still the original path
		auto xml = GetRTXmlResponse("<?xml version=\"1.0\" ?><methodCall><methodName>d.base_path</methodName><params><param><value><string>" ~ hash ~ "</string></value></param></params></methodCall>");
		
		if (xml is null) {
			logInfo("Failed to connect to rtorrent");
			return;
		}
		
		SXPreSingle(xml);
		auto sourcePath = expandTilde(decode(SXString(xml)));

		try {
			copyParallel(sourcePath, targetPath);
			debug logWarn("rm -rf %s", sourcePath);
			if (isDir(sourcePath))
				rmdirRecurse(sourcePath);
			else
				remove(sourcePath);
		} catch (Exception ex) {
			logError("Error copying/deleting files");
		}
		// <<<
	}

	client.Stm.write([ cast(ubyte)0x01 ]);
}

void ActGenerateISFAToken(ref Client client) {
	import std.parallelism : parallel;
	import vibe.crypto.cryptorand : SystemRNG;
	import std.stdio : File;
	import vibe.core.core : setTimer;
	
	auto path = expandTilde(ReadString(client.Stm));
	
	if (!exists(path))
		mkdir(path);
	
	ubyte[] toWrite;
	try {
		auto filesCheck = dirEntries(path, SpanMode.depth).filter!(x => x.isFile);
		
		foreach (f; parallel(filesCheck))
		    File(f.name, "rb").close();
	} catch (Exception ex) {
		WriteIntToBuffer(toWrite, 0);
		client.Stm.write(toWrite);
		return;
	}
	
	auto rng = new SystemRNG();
	ubyte[32] token;
	rng.read(token);
	
	isfaTokens[token] = path;
	auto b = ((cast(ubyte[])token) in isfaTokens);
	logDebug("Generated token: %s (%d) -> %s", token[0 .. $], isfaTokens.length, *b);

	WriteIntToBuffer(toWrite, 1);
	client.Stm.write(toWrite);
	client.Stm.write(token);

	setTimer(Config.ISFATokenTimeout, {
		logDebug("ISFA: token %s timeout", token[0 .. $]);
		isfaTokens.remove(token);
	}, false);
}

void ActAcceptISFA(ref Client client) {
	import std.path : relativePath, buildNormalizedPath;
	auto keyLength = StreamReadTBytes!ubyte(client.Stm)[0];
	
	ubyte[32] token = StreamRead(client.Stm, keyLength);
	auto count = T2B!uint(StreamReadTBytes!uint(client.Stm));
	
	ubyte[] toWrite;
	
	auto savePath = ((cast(ubyte[])token) in isfaTokens);
	
	if (savePath is null) {
		logInfo("ISFA: path null (recv token: %s, tokens: %d)", token[0 .. $], isfaTokens.length);
		
		WriteBinToBuffer!ubyte(toWrite, 0);
		client.Stm.write(toWrite);
		return;
	}
	
	if (!exists(*savePath))
		mkdir(*savePath);
	
	logInfo("ISFA: notif OK");
	WriteBinToBuffer!ubyte(toWrite, 1);
	client.Stm.write(toWrite);
	
	for (auto x = 0;x < count;x++) {
		logDebug("ISFA: PRE");
		auto pathLen = T2B!uint(StreamReadTBytes!uint(client.Stm));
		logDebug("ISFA: pathLen: %s", pathLen);
		
		if (pathLen > 512) {
			logWarn("ISFA: path length exceeds 512");
			return;
		}
		
		auto path = cast(string)StreamRead(client.Stm, pathLen);
		logDebug("ISFA: path: %s", path);
		auto size = T2B!ulong(StreamReadTBytes!ulong(client.Stm));
		logDebug("ISFA: size: %d", size);
		
		logDebug("ISFA: recv 3");
		
		if (buildNormalizedPath(path) != path || path.startsWith("/")) {
			logWarn("ActAcceptISFA: Received path is not normalized ('%s' vs '%s')", buildNormalizedPath(path), path);
			return;
		}
		logDebug("ISFA: pre open");
		auto fs = openFile(buildPath(*savePath, path), FileMode.createTrunc);
		logDebug("ISFA: post open, pre recv");
		try pipe(client.Stm, fs, size);
		catch (Exception ex) {
			logWarn("ActAcceptISFA: %s", ex.msg);
			return;
		}
		logDebug("ISFA: recveived %s", buildPath(*savePath, path));
	}
}

void ActSendISFA(ref Client client) {
	import std.path : baseName;
	import utils : folderSize;
	import std.stdio : File;
	import std.math : floor;
	
	auto tokenSize = ReadInt!uint(client.Stm);
	auto recvToken = StreamRead(client.Stm, tokenSize);
	auto targetHost = ReadString(client.Stm);
	auto targetPort = ReadInt!ushort(client.Stm);
	auto pathToSend = expandTilde(ReadString(client.Stm));
	auto ssl = ReadInt!bool(client.Stm);
	ubyte[] sslHash;
	
	if (ssl)
		sslHash = StreamRead(client.Stm, ReadInt!ushort(client.Stm));
	
	ubyte[] toWrite;
	
	if (recvToken.length > 255) {
		logInfo("Invalid token");
		WriteIntToBuffer(toWrite, 0);
		WriteStringToBuffer(toWrite, "Invalid token");
		client.Stm.write(toWrite);
		return;
	}
	
	Stream conn;
	auto tcp = connectTCP(targetHost, targetPort);
	if (!tcp.connected) {
		logInfo("Failed to connect to remote target");
		WriteIntToBuffer(toWrite, 0);
		WriteStringToBuffer(toWrite, "Failed to connect to remote target");
		client.Stm.write(toWrite);
		return;
	}
	
	if (ssl) {
		auto sslCtx = createTLSContext(TLSContextKind.client, TLSVersion.tls1_2);
		
		sslCtx.peerValidationMode(TLSPeerValidationMode.requireCert);
		// HACKHACK HACKHACK TODO -->>> sslHash
		
		conn = createTLSStream(tcp, sslCtx, targetHost);
	} else
		conn = tcp;
	
	toWrite ~= [ 0xFF, 0x33, 0xB0, 0xE6, 0xF5 ];
	toWrite ~= [ cast(ubyte)recvToken.length ];
	toWrite ~= recvToken;
	logInfo("ISFA: magic + token (%s)", recvToken[0 .. $]);
	
	auto dir = isDir(pathToSend);
	ulong totalSize;
	string[] files;
	if (dir) {
		files = dirEntries(pathToSend, SpanMode.depth).filter!(x => x.isFile).map!(x => x.name).array();
		totalSize = folderSize(pathToSend);
	} else {
		files ~= pathToSend;
		totalSize = File(pathToSend).size;
	}
	
	WriteBinToBuffer!uint(toWrite, cast(uint)files.length);
	conn.write(toWrite);
	toWrite.length = 0;
	
	auto status = StreamReadTBytes!ubyte(conn)[0];
	if (status != 1) {
		logInfo("Remote target reported a failure");
		WriteIntToBuffer(toWrite, 0);
		WriteStringToBuffer(toWrite, "Remote target reported a failure");
		client.Stm.write(toWrite);
		return;
	}
	logDebug("ISFA send proceed");

	WriteIntToBuffer(toWrite, 1);
	WriteIntToBuffer(toWrite, totalSize);
	client.Stm.write(toWrite);
	toWrite.length = 0;
	ulong clientProgress = 0;
	
	foreach (f; files) {
		FileStream fs;
		try fs = openFile(f, FileMode.read);
		catch (Exception ex) {
			logError("ActSendISFA: %s", ex.msg);
			WriteIntToBuffer(toWrite, 0);
			WriteStringToBuffer(toWrite, "Local failure: " ~ ex.msg);
			client.Stm.write(toWrite);
			return;
		}
	
		string chompPath;
		if (dir) {
			chompPath = f.chompPrefix(pathToSend);
			if (chompPath.length >= 1 && chompPath[0] == '/')
				chompPath = chompPath[1 .. $];
		} else
			chompPath = baseName(f);
	
		WriteBinToBuffer!uint	(toWrite, cast(uint)chompPath.length);
		toWrite ~=				cast(ubyte[])chompPath;
		WriteBinToBuffer!ulong	(toWrite, fs.size);
		conn.write(toWrite);
		toWrite.length = 0;
		
		ubyte[4096] buff;
		
		try {
			for (ulong x = 0;x < floor(cast(double)fs.size / 4096);x++) {
				fs.read(buff);
				conn.write(buff);
				if (x % 1000 == 0) {
					WriteIntToBuffer(toWrite, clientProgress + cast(ulong)(x * 4096));
					try {
						client.Stm.write(toWrite);
					} catch (Exception ex) { }
					toWrite.length = 0;
				}
			}
			auto finalBuff = new ubyte[fs.size % 4096];
			fs.read(finalBuff);
			conn.write(finalBuff);
			
			WriteIntToBuffer(toWrite, clientProgress + cast(ulong)(fs.size));
			try {
				client.Stm.write(toWrite);
			} catch (Exception ex) { }
			toWrite.length = 0;
			clientProgress += fs.size;
		} catch (Exception ex) {
			logError("ActSendISFA (2): %s", ex.msg);
			return;
		}
	}
}

void ActGetServerStatus(ref Client client) {
	import std.process;
	import std.regex;
	
	/*
	 * Stage 1
	 */
	auto df = execute("df");
	assert(df.status == 0);
	
	auto outp = df.output[df.output.indexOf("\n")+1 .. $];
	
	auto dfRegex = ctRegex!`(.+?) +(\d+) +(\d+)( +\d+){2}% +(.+?)\n`;
	auto cpuRegex = ctRegex!`(\d+\.\d+) (\d+\.\d+) (\d+\.\d+)`;
	
	ubyte[] drv;
	ushort c = 0;
	
	foreach (m; matchAll(outp, dfRegex)) {
		auto name = m[1];
		if (name == "udev" || name == "tmpfs")
			continue;
			
		auto blocks = to!ulong(m[2]);
		auto used = to!ulong(m[3]);
		auto mount = m[5];
		
		WriteStringToBuffer(drv, name);
		WriteIntToBuffer(drv, blocks);
		WriteIntToBuffer(drv, used);
		WriteStringToBuffer(drv, mount);
		c++; // eww
	}
	
	ubyte[] toWrite;
	WriteIntToBuffer(toWrite, c);
	toWrite ~= drv;
	
	/*
	 * Stage 2
	 */
	auto cpu = readText("/proc/loadavg");
	auto matches = matchAll(cpu, cpuRegex);
	auto cpu1 = to!float(matches.front[1]);
	auto cpu5 = to!float(matches.front[2]);
	auto cpu15 = to!float(matches.front[3]);
	
	WriteIntToBuffer(toWrite, *(cast(uint*)&cpu1));
	WriteIntToBuffer(toWrite, *(cast(uint*)&cpu5));
	WriteIntToBuffer(toWrite, *(cast(uint*)&cpu15));
	
	/*
	 * Stage 3
	 */

	auto xml = GetRTXmlResponse("<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>system.client_version</string></value></member><member><name>params</name><value><array><data /></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>system.library_version</string></value></member><member><name>params</name><value><array><data /></array></value></member></struct></value></data></array></value></param></params></methodCall>");
	
	if (xml is null) {
		logInfo("Failed to connect to rtorrent");
		return;
	}
	
	SXPre(xml);
	auto clientVer = SXString(xml);
	SXMid(xml);
	auto libraryVer = SXString(xml);
	
	WriteStringToBuffer(toWrite, clientVer);
	WriteStringToBuffer(toWrite, libraryVer);
	
	/*
	 * Stage 4
	 */
	WriteStringToBuffer(toWrite, readText("/etc/hostname").chomp());
	
	// ---------------------
	client.Stm.write(toWrite);
}
