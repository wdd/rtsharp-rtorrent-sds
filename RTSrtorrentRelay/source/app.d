import vibe.d;
import onyx.bundle;
import config;
import handle;
import actions;
import sendEx;
import std.conv;
import std.algorithm;
import core.time;
import core.memory;


import etc.linux.memoryerror;


const string VERSION = "0.1.0";
shared immutable ulong I_VERSION;


__gshared TLSContext SSLCtx;


shared static this()
{
	import std.c.stdlib : exit;


	bool ver;
	readOption("version", &ver, "Prints version");
	if (ver) {
		logInfo("RT#-Relay server v%s\n\n", VERSION);
		exit(1);
		return;
	}


	static if (is(typeof(registerMemoryErrorHandler)))
		registerMemoryErrorHandler();


	auto vSplits = split(VERSION, '.');
	I_VERSION = (to!ulong(vSplits[0]) << 40) + (to!ulong(vSplits[1]) << 32) + to!uint(vSplits[2]);


	auto config = new immutable Bundle("config.conf");
	try { Config.IPV4Listen = config.value("Server", "IPV4Listen"); } catch (Exception ex) { }
	try { Config.IPV6Listen = config.value("Server", "IPV6Listen"); } catch (Exception ex) { }
	Config.ServerPort = config.value!ushort("Server", "NonSSLServerPort");
	Config.AccessPassword = config.value("Server", "AccessPassword");
	if (Config.AccessPassword == "CHANGE_ME") {
		logFatal("Please change access password.");
		exit(1);
	}
	if (Config.AccessPassword.length < 15) {
		logFatal("Please choose a longer access password (>=15 chars). It's for your own safety. Use a SECURE random password generator.");
		exit(1);
	}
	if (Config.AccessPassword[0] == '\xFF') {
		logFatal("Sorry, password cannot start with 0xFF");
		exit(1);
	}
	
	Config.SCGIHost = config.value("Remote", "SCGIHost");
	try {
		Config.SCGIPort = config.value!ushort("Remote", "SCGIPort");
	} catch (Exception ex) {
		logInfo("No SCGI port detected, defaulting to UNIX socket...");
		version (Windows) {
			logError("Sorry, you're on windows");
			exit(1);
		} else {
			if (Config.SCGIHost.length > 103) {
				logError("UNIX socket path length cannot be greater than 103");
				exit(1);
			}
			Config.SCGIPort = 0;
		
			import core.sys.posix.sys.un;
			import core.stdc.string : strcpy;
	
			unixAddr = NetworkAddress.init;
			unixAddr.family = AddressFamily.UNIX;
			unixAddr.sockAddrUnix.sun_family = AddressFamily.UNIX;
			strcpy(cast(char*)unixAddr.sockAddrUnix.sun_path.ptr, Config.SCGIHost.toStringz());
			static if (is(typeof(sockaddr_un.sun_len))) {
				unixAddr.sockAddrUnix.sun_len = Config.SCGIHost.length + 1 + short.sizeof;
				// HACKHACK
			}
	
			unixSockLock = new TaskMutex();
		}
	}
	Config.SSLEnabled = to!bool(config.value!int("SSL", "Enabled"));
	if (Config.SSLEnabled) {
		Config.SSLPrivateKey = config.value("SSL", "PrivateKey");
		Config.SSLPublicKey = config.value("SSL", "PublicKey");
		Config.SSLCipherList = config.value("SSL", "CipherList");
		Config.SSLServerPort = config.value!ushort("SSL", "SSLServerPort");
	}
	
	Config.ISFATokenTimeout = config.value!int("Server", "ISFATokenTimeout").seconds;
	
	auto closeOnInvalidPassword = to!bool(config.value!int("Server", "CloseOnInvalidPassowrd"));


	TCPListenOptions options =
		TCPListenOptions.distribute |
		(closeOnInvalidPassword ? cast(TCPListenOptions)0 : TCPListenOptions.disableAutoClose) |
		TCPListenOptions.reusePort;


	logInfo("Listening on:");


	if (Config.IPV4Listen != "") {
		if (Config.ServerPort != 0) {
			listenTCP(Config.ServerPort, (conn) => TCPHandle(conn, conn), Config.IPV4Listen, options);
			logInfo("%s", Config.IPV4Listen);
		}
		if (Config.SSLEnabled) {
			listenTCP(Config.SSLServerPort, (conn) => TCPHandleSSL(conn), Config.IPV4Listen, options);
			logInfo("%s SSL (%s, %s)", Config.IPV4Listen, Config.SSLPublicKey, Config.SSLPrivateKey);
		}
	}
	if (Config.IPV6Listen != "") {
		if (Config.ServerPort != 0) {
			listenTCP(Config.ServerPort, (conn) => TCPHandle(conn, conn), Config.IPV6Listen, options);
			logInfo("%s", Config.IPV6Listen);
		}
		if (Config.SSLEnabled) {
			listenTCP(Config.SSLServerPort, (conn) => TCPHandleSSL(conn), Config.IPV6Listen, options);
			logInfo("%s SSL (%s, %s)", Config.IPV6Listen, Config.SSLPublicKey, Config.SSLPrivateKey);
		}
	}


	if (Config.SSLEnabled) {
		SSLCtx = createTLSContext(TLSContextKind.server, TLSVersion.tls1_2);
		SSLCtx.useCertificateChainFile(Config.SSLPublicKey);
		SSLCtx.usePrivateKeyFile(Config.SSLPrivateKey);
		SSLCtx.setCipherList(Config.SSLCipherList);
		logDebug("SSL Set");
	}
	
	bool comm;
	readOption("tcfill", &comm, "Fetches torrent comments");
	if (comm) {
		import std.file : read, exists;
		import std.xml : decode, encode;
		import actions, xmls : XmlSystemSettings;
		import bencode;
		
		string[string] allComments;
		
		string xml = XmlSystemSettings([ "get_session" ]);
		
		auto resXml = GetRTXmlResponse(xml);
		
		if (resXml is null)
			return;
		
		SXPre(resXml);
	    auto sessionPath = decode(SXString(resXml));
		
		resXml = GetRTXmlResponse("<?xml version=\"1.0\" ?><methodCall><methodName>d.multicall</methodName><params><param><value><string>main</string></value></param><param><value><string>d.get_hash=</string></value></param></params></methodCall>");
		
		if (resXml is null)
			return;
		
		SXPre(resXml);
		while (mixin(SXToEnd!(resXml))) {
			auto sHash = SXString(resXml);
			
			string torrentPath = buildPath(sessionPath, sHash ~ ".torrent");
			logInfo(torrentPath);
			
	        if (!exists(torrentPath)) {
	        	logError("%s doesn't exist", torrentPath);
	        	SXMid(resXml);
	        	continue;
	        }
	        
	        auto file = cast(ubyte[])read(torrentPath);
			auto torrent = bencodeParse(file);
			string comment = "";
			try {
				auto pCmt = torrent["comment"];
				if (pCmt !is null)
					comment = *torrent["comment"].str();
			} catch (Exception ex) {
				logError("Failed to get comment of %s: %s", torrentPath, ex.msg);
			}
			
			logInfo("%s -> %s", sHash, comment);
			allComments[sHash] = comment;
			
			SXMid(resXml);
		}
		
		xml = "<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>";
		foreach (item; allComments.byKeyValue)
			xml ~= "<value><struct><member><name>methodName</name><value><string>d.set_custom2</string></value></member><member><name>params</name><value><array><data><value><string>" ~ item.key ~ "</string></value><value><string>VRS24mrker" /* < rutorrent compability */ ~ encode(item.value) ~ "</string></value></data></array></value></member></struct></value>";
		resXml = GetRTXmlResponse(xml ~ "</data></array></value></param></params></methodCall>");
		
		if (resXml is null)
			return;
		
		logInfo("Return: " ~ resXml);
		
		exit(0);
		return;
	}


	logDebug("Setting actions...");
	auto xml = GetRTXmlResponse("<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>system.method.set_key</string></value></member><member><name>params</name><value><array><data><value><string>event.download.finished</string></value><value><string>seedingtimertsrelay</string></value><value><string>d.set_custom=seedingtime,\"$execute_capture={date,+%s}\"</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>system.method.set_key</string></value></member><member><name>params</name><value><array><data><value><string>event.download.inserted_new</string></value><value><string>addtimertsrelay</string></value><value><string>d.set_custom=addtime,\"$execute_capture={date,+%s}\"</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>set_xmlrpc_size_limit</string></value></member><member><name>params</name><value><array><data><value><i8>67108863</i8></value></data></array></value></member></struct></value></data></array></value></param></params></methodCall>");
	logDebugV("Actions return: %s", xml);
	logDebug("Actions set");
}


void TCPHandleSSL(TCPConnection conn) {
	Stream stm = conn;

	stm = createTLSStream(stm, SSLCtx, TLSStreamState.accepting);
	logTrace("Created stream SSL");
	runTask(() => TCPHandle(stm, conn));
	logTrace("Handled SSL");
}


void TCPHandle(Stream stm, TCPConnection tcp)
{
	Client client;


	ubyte[1] pref;
	ubyte[] data;
	stm.read(pref);
	
	client.Stm = stm;
	
	if (pref.length == 1 && pref[0] == 0xFF) {
		data.length = 4;
		stm.read(data);
		if (data.length == 4 && data[0 .. 4] == "\x33\xB0\xE6\xF5") {
			ActAcceptISFA(client);
			return;
		}
	}
	
	data.length = Config.AccessPassword.length - 1;
	stm.read(data);
	data = pref ~ data;


	if (data != Config.AccessPassword) {
		logDebug("Invalid password from %s (got %s)", tcp.remoteAddress.toString(), data[0..$]);
		return; // Leave it hanging (if config option is not set), if we close connection immediately after wrong password, user will know password length
	}


	HandleClient(client);
}

