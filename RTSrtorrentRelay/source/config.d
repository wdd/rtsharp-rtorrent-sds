module config;

import vibe.d;
import std.string;

shared static class Config {
shared: static:
	string IPV4Listen;
	string IPV6Listen;
	ushort ServerPort;
	string AccessPassword;
	string SCGIHost;
	ushort SCGIPort;
	bool SSLEnabled;
	string SSLPrivateKey;
	string SSLPublicKey;
	string SSLCipherList;
	ushort SSLServerPort;
	Duration ISFATokenTimeout;
}
