module handle;

import std.bitmanip;
import std.format;
import std.algorithm;
import std.algorithm.comparison;
import std.socket;
import std.typecons;
import std.datetime;
import std.conv;
import std.container;
import std.range;
import std.variant;
import vibe.core.concurrency;

import config;
import xpaths;
import sendEx;
import xmls;
import actions;

import vibe.d;

shared string[ubyte[]] isfaTokens;

__gshared NetworkAddress unixAddr;
__gshared TaskMutex unixSockLock;
__gshared int debugSimul = 0;

string GetRTXmlResponse(string Xml)
{
	string mid = format("CONTENT_LENGTH\x00%d\x00SCGI\x00" ~ "1\x00", Xml.length);
    string all = format("%d:%s,%s", mid.length, mid, Xml);
	string res;
	debug logDebugV("rtorrent write %s", all);
	
	if (Config.SCGIPort != 0) {
		auto tcp = connectTCP(Config.SCGIHost, Config.SCGIPort);
		if (!tcp.connected) {
			logDebug("Failed to connect to rtorrent. Did you open SCGI on %s:%d?", Config.SCGIHost, Config.SCGIPort);
			return null;
		}
		
		try {
			tcp.write(all);
			res = cast(string)(readAll(tcp));
			tcp.close();
		} catch (Exception ex) {
			logWarn("Failed to communicate with rtorrent: %s", ex.msg);
			logDebugV("Tried to write: %s", all);
		}
	} else {
		import std.exception;
		try {
			unixSockLock.lock();
            debugSimul++;
            assert(debugSimul == 1);
			int waitFree = 0;
			TCPConnection conn;
			while (waitFree < 40) {
				try {
					conn = connectTCP(unixAddr);
					break;
				} catch (ErrnoException ex) {
					if (ex.msg == "Failed to bind socket. (Address already in use)") {
						waitFree++;
						logInfo("RETRY %d", waitFree);
						sleep(50.msecs);
					}
				}
			}
			if (!conn.connected) {
				logDebug("Failed to connect to rtorrent (unix socket). Did you open SCGI on %s?", Config.SCGIHost);
				return null;
			}
			conn.readTimeout = 1.seconds;
			conn.write(all);
			res = cast(string)(readAll(conn));
			conn.close();
			//sleep(100.msecs); // HACKHACK HACKHACK
		} finally {
            debugSimul--;
			unixSockLock.unlock();
		}
	}
	
	debug logDebugV("rtorrent response %s", res);


    return res.replace("\r\n", "").replace("\n", "");
}

void HandleClient(ref Client client)
{
    auto stm = client.Stm;

    while (1) {
	    SCGI_ACTION action = cast(SCGI_ACTION)(ReadInt!ubyte(stm));

        string xml = "", xmlRes;
        XML_RET xmlRet;
        ptrdiff_t headerEnd;
        ubyte[] res;

		TCPConnection conn;

		logDiagnostic("ACTION: %d", action);

	    /*final*/ switch (action) {
			case SCGI_ACTION.VERSION:
				break;
		    case SCGI_ACTION.GET_ALL_TORRENT_ROWS:
                xmlRet = XmlGetTorrents(client);
			    break;
            case SCGI_ACTION.LOAD_TORRENT_RAW:
                xmlRet = XmlLoadRaw(client);
			    break;
            case SCGI_ACTION.START_TORRENTS_MULTI:
                xmlRet = XmlStartTorrentsMulti(client);
                break;
            case SCGI_ACTION.STOP_TORRENTS_MULTI:
                xmlRet = XmlStopTorrentsMulti(client);
                break;
            case SCGI_ACTION.PAUSE_TORRENTS_MULTI:
                xmlRet = XmlPauseTorrentsMulti(client);
                break;
            case SCGI_ACTION.FORCE_RECHECK_TORRENTS_MULTI:
                xmlRet = XmlForceRecheckTorrentsMulti(client);
                break;
            case SCGI_ACTION.REANNOUNCE_TORRENTS_MULTI:
                xmlRet = XmlReannounceTorrentsMulti(client);
                break;
            case SCGI_ACTION.ADD_PEER_TORRENT:
                xmlRet = XmlAddPeerTorrent(client);
                break;
            case SCGI_ACTION.SET_LABEL_TORRENTS_MULTI:
                xmlRet = XmlSetLabelTorrentsMulti(client);
                break;
            case SCGI_ACTION.SET_PRIORITY_TORRENTS_MULTI:
                xmlRet = XmlSetPriorityTorrentsMulti(client);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_MULTI:
                xmlRet = XmlRemoveTorrentsMulti(client);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_PLUS_DATA_MULTI:
                xmlRet = XmlRemoveTorrentsPlusDataMulti(client);
                break;
            case SCGI_ACTION.GET_TORRENT_FILES_MULTI:
                xmlRet = XmlGetTorrentFilesMulti(client);
                break;
            case SCGI_ACTION.GET_TORRENT_PEERS:
                xmlRet = XmlGetTorrentPeers(client);
                break;
			case SCGI_ACTION.GET_TORRENT_FILES:
                xmlRet = XmlGetTorrentFiles(client);
                break;
			case SCGI_ACTION.CUSTOM_CMD:
				xmlRet = XmlExecCustomCommand(client);
				break;
			case SCGI_ACTION.GET_SETTINGS:
				xmlRet = XmlGetSettings(client);
				break;
			case SCGI_ACTION.SET_SETTINGS:
				xmlRet = XmlSetSettings(client);
				break;
            case SCGI_ACTION.TRANSFER_FILE:
                xmlRet = XmlTransferFile();
                break;
			case SCGI_ACTION.TOGGLE_TRACKERS_MULTI:
				xmlRet = XmlToggleTrackersMulti(client);
				break;
			case SCGI_ACTION.KICK_PEER:
				xmlRet = XmlKickPeer(client);
				break;
			case SCGI_ACTION.BAN_PEER:
				xmlRet = XmlBanPeer(client);
				break;
			case SCGI_ACTION.TOGGLE_SNUB_PEER:
				xmlRet = XmlToggleSnubPeer(client);
				break;
			case SCGI_ACTION.SET_FILE_PRIORITY:
				xmlRet = XmlSetFilePriority(client);
				break;
			case SCGI_ACTION.SET_FILE_DOWNLOAD_STRATEGY:
				xmlRet = XmlSetFileDownloadStrategy(client);
				break;
			case SCGI_ACTION.FILE_MEDIAINFO:
				xmlRet = XmlFileMediaInfo(client);
				break;
			case SCGI_ACTION.DOWNLOAD_FILE:
				xmlRet = XmlDownloadFile(client);
				break;
			case SCGI_ACTION.EDIT_TORRENT:
				xmlRet = XmlEditTorrent(client);
				break;
			case SCGI_ACTION.LIST_DIRECTORIES:
				xmlRet = XmlDirectoryListing(client);
				break;
			case SCGI_ACTION.CREATE_DIRECTORY:
				xmlRet = XmlCreateDirectory(client);
				break;
			case SCGI_ACTION.I_FR_GET_MTIMES:
				xmlRet = XmlIFastResumeGetMTimes(client);
				break;
			case SCGI_ACTION.LOAD_TORRENT_STRING:
				xmlRet = XmlLoadString(client);
				break;
			case SCGI_ACTION.GET_TORRENT_TRACKERS:
				xmlRet = XmlGetTorrentTrackers(client);
				break;
			case SCGI_ACTION.SET_TORRENT_DATA_DIR:
				xmlRet = XmlSetTorrentDataDir(client);
				break;
			case SCGI_ACTION.GENERATE_ISFA_TOKEN:
				xmlRet = XmlGenerateISFAToken(client);
				break;
			case SCGI_ACTION.SEND_ISFA:
				xmlRet = XmlSendISFA(client);
				break;
			case SCGI_ACTION.GET_SERVER_STATUS:
				xmlRet = XmlGetServerStatus(client);
				break;
		    default:
			    goto G_nextCommand;
	    }
		xml = xmlRet.Xml;
		

		if (xml.length != 0) {
			xmlRes = GetRTXmlResponse(xml);

			if (xmlRes is null)
				return;
			
			if (action != SCGI_ACTION.GET_ALL_TORRENT_ROWS/* && action != SCGI_ACTION.GET_TORRENT_PEERS*/)
				logDebug("resXml: %s", xmlRes);
		}

	    /*final*/ switch (action) {
			case SCGI_ACTION.VERSION:
				ActVersion(client);
				break;
		    case SCGI_ACTION.GET_ALL_TORRENT_ROWS:
                ActGetTorrents(client, xmlRet.Data, xmlRes);
			    break;
            case SCGI_ACTION.LOAD_TORRENT_RAW:
                ActLoadRaw(client, xmlRes);
			    break;
            case SCGI_ACTION.START_TORRENTS_MULTI:
                ActStartTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.STOP_TORRENTS_MULTI:
                ActStopTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.PAUSE_TORRENTS_MULTI:
                ActPauseTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.FORCE_RECHECK_TORRENTS_MULTI:
                ActForceRecheckTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.REANNOUNCE_TORRENTS_MULTI:
                ActReannounceTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.ADD_PEER_TORRENT:
                ActAddPeerTorrent(client, xmlRes);
                break;
            case SCGI_ACTION.SET_LABEL_TORRENTS_MULTI:
                ActSetLabelTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.SET_PRIORITY_TORRENTS_MULTI:
                ActSetPriorityTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_MULTI:
                ActRemoveTorrentsMulti(client, xmlRes);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_PLUS_DATA_MULTI:
                ActRemoveTorrentsPlusDataMulti(client, xmlRes);
                break;
            case SCGI_ACTION.GET_TORRENT_FILES_MULTI:
                ActGetTorrentFilesMulti(client, xmlRet.Data);
                break;
            case SCGI_ACTION.GET_TORRENT_PEERS:
                ActGetTorrentPeers(client, xmlRes);
                break;
			case SCGI_ACTION.GET_TORRENT_FILES:
				ActGetTorrentFiles(client, xmlRes, xmlRet.Data);
                break;
			case SCGI_ACTION.CUSTOM_CMD:
				ActExecCustomCommand(client, xmlRes);
				break;
			case SCGI_ACTION.GET_SETTINGS:
				ActGetSettings(client, xmlRes);
				break;
            case SCGI_ACTION.SET_SETTINGS:
                ActSetSettings(client, xmlRes);
                break;
            case SCGI_ACTION.TRANSFER_FILE:
                ActTransferFile(client);
                break;
			case SCGI_ACTION.TOGGLE_TRACKERS_MULTI:
				ActToggleTrackersMulti(client, xmlRes);
				break;
			case SCGI_ACTION.KICK_PEER:
				ActKickPeer(client, xmlRes);
				break;
			case SCGI_ACTION.BAN_PEER:
				ActBanPeer(client, xmlRes);
				break;
			case SCGI_ACTION.TOGGLE_SNUB_PEER:
				ActToggleSnubPeer(client, xmlRes);
				break;
			case SCGI_ACTION.SET_FILE_PRIORITY:
				ActSetFilePriority(client, xmlRes);
				break;
			case SCGI_ACTION.SET_FILE_DOWNLOAD_STRATEGY:
				ActSetFileDownloadStrategy(client, xmlRes);
				break;
			case SCGI_ACTION.FILE_MEDIAINFO:
				ActFileMediaInfo(client, xmlRes);
				break;
			case SCGI_ACTION.DOWNLOAD_FILE:
				ActDownloadFile(client, xmlRes);
				break;
			case SCGI_ACTION.EDIT_TORRENT:
				ActEditTorrent(client, xmlRes);
				break;
			case SCGI_ACTION.LIST_DIRECTORIES:
				ActDirectoryListing(client);
				break;
			case SCGI_ACTION.CREATE_DIRECTORY:
				ActCreateDirectory(client);
				break;
			case SCGI_ACTION.I_FR_GET_MTIMES:
				ActIFastResumeGetMTimes(client);
				break;
			case SCGI_ACTION.LOAD_TORRENT_STRING:
				ActLoadString(client, xmlRes);
				break;
			case SCGI_ACTION.GET_TORRENT_TRACKERS:
				ActGetTorrentTrackers(client, xmlRes);
				break;
			case SCGI_ACTION.SET_TORRENT_DATA_DIR:
				ActSetTorrentDataDir(client, xmlRet.Data, xmlRes);
				break;
			case SCGI_ACTION.GENERATE_ISFA_TOKEN:
				ActGenerateISFAToken(client);
				break;
			case SCGI_ACTION.SEND_ISFA:
				ActSendISFA(client);
				break;
			case SCGI_ACTION.GET_SERVER_STATUS:
				ActGetServerStatus(client);
				break;
		    default:
			    break;
	    }

        logDiagnostic("---------------------- command end ----------------------");
        G_nextCommand:;
    }
}
