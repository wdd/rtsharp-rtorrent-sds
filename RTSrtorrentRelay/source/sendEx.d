module sendEx;

import std.bitmanip;
import std.socket;
import std.variant;
import std.algorithm;
import core.stdc.string;
import core.stdc.limits;
import vibe.d;
debug import std.traits;

enum SCGI_ACTION : ubyte {
	VERSION = 0,
	GET_ALL_TORRENT_ROWS = 1,
    LOAD_TORRENT_RAW = 2,
    START_TORRENTS_MULTI = 3,
    PAUSE_TORRENTS_MULTI = 4,
    STOP_TORRENTS_MULTI = 5,
    FORCE_RECHECK_TORRENTS_MULTI = 6,
    REANNOUNCE_TORRENTS_MULTI = 7,
    ADD_PEER_TORRENT = 8,
    SET_LABEL_TORRENTS_MULTI = 9,
    SET_PRIORITY_TORRENTS_MULTI = 10,
    REMOVE_TORRENTS_MULTI = 11,
    REMOVE_TORRENTS_PLUS_DATA_MULTI = 12,
    GET_TORRENT_FILES_MULTI = 13,
    GET_TORRENT_PEERS = 14,
	GET_TORRENT_FILES = 15,
	CUSTOM_CMD = 16,
    GET_SETTINGS = 17,
	SET_SETTINGS = 18,
    TRANSFER_FILE = 19,
	TOGGLE_TRACKERS_MULTI = 20,
	KICK_PEER = 21,
	BAN_PEER = 22,
	TOGGLE_SNUB_PEER = 33,
	SET_FILE_PRIORITY = 34,
	SET_FILE_DOWNLOAD_STRATEGY = 35,
	FILE_MEDIAINFO = 36,
	DOWNLOAD_FILE = 37,
	EDIT_TORRENT = 38,
	LIST_DIRECTORIES = 39,
	CREATE_DIRECTORY = 40,
	I_FR_GET_MTIMES = 41,
	LOAD_TORRENT_STRING = 42,
	GET_TORRENT_TRACKERS = 43,
	SET_TORRENT_DATA_DIR = 44,
	GENERATE_ISFA_TOKEN = 45,
	SEND_ISFA = 46,
	GET_SERVER_STATUS = 47,
	CUSTOM = 255
}


enum TORRENT_STATE : ushort {
	NONE = 0,
	PRIVATE = 1 << 0,
	DOWNLOADING = 1 << 1,
	SEEDING = 1 << 2,
	HASHING = 1 << 3,
	PAUSED = 1 << 4,
	STOPPED = 1 << 5,
	COMPLETE = 1 << 6,
	ACTIVE = 1 << 7,
	INACTIVE = 1 << 8,
	ERRORED = 1 << 9
}

enum TORRENT_PRIORITY : byte {
	NA = -1,
	HIGH = 3,
	NORMAL = 2,
	LOW = 1,
	OFF = 0
}

enum PEER_FLAGS : ubyte {
    INCOMING = 1 << 0,
    ENCRYPTED = 1 << 1,
    SNUBBED = 1 << 2,
    OBFUSCATED = 1 << 3
}

enum TRACKER_STATUS : ubyte {
	ACTIVE = 1 << 0,
	NOT_ACTIVE = 1 << 1,
	NOT_CONTACTED_YET = 1 << 2,
	DISABLED = 1 << 3,
	ENABLED = 1 << 4
}

struct TRACKER {
	union UUri {
		ushort UriCode;
		string Uri;
	};
	UUri Uri;
    public TRACKER_STATUS Status;
    public uint Seeders;
    public uint Peers;
    public uint Downloaded;
    public ulong LastUpdated;
    public uint Interval;
    public ubyte StatusMsg;
}

struct PEER {
	public ubyte[20] ID;
	public Variant Addr;
	public string Client;
	public PEER_FLAGS Flags;
	public ulong Downloaded;
	public ulong Uploaded;
	public uint DLSpeed;
	public uint UPSpeed;
    public ubyte Done;
}

struct TORRENT {
	string Name;
	TORRENT_STATE State;
	ulong Size;
	ulong Downloaded;
	ulong Uploaded;
	uint DLSpeed;
	uint UPSpeed;
	string Label;
	string Comment;
	string Path;
	uint PeersConnected;
	uint PeersTotal;
	uint SeedersConnected;
	uint SeedersTotal;
	TORRENT_PRIORITY Priority;
	ulong CreatedOn;
	ulong AddedOn;
	ulong FinishedOn;
	TRACKER*[] Trackers;
	string StatusMsg;
	uint ChunkSize;
	ulong WastedBytes;

	PEER[] PeerList;
}

enum FILE_PRIORITY : byte {
	HIGH = 2,
	NORMAL = 1,
	DONT_DOWNLOAD = 0
}

enum FILE_DOWNLOAD_STRATEGY : byte {
	NORMAL = 0,
	LEADING_CHUNK_FIRST = 1,
	TRAILING_CHUCK_FIRST = 2
}

struct FILE {
	string Path;
	ulong Size;
	ulong DownloadedChunks;
	FILE_PRIORITY Priority;
	FILE_DOWNLOAD_STRATEGY DownloadStrategy;
}

struct Client {
    uint LastTag;
    TORRENT[ubyte[20]] LastTorrentList;
    Stream Stm;
}

int BitCount(uint n) {
	pragma(inline);
	uint tmp = n - ((n >> 1) & 0xDB6DB6DB) - ((n >> 2) & 0x49249249);
	return ((tmp + (tmp >> 3)) & 0xC71C71C7) % 63;
}

void WriteStringToBuffer(ref ubyte[] Buff, string In) {
	pragma(inline);
    WriteIntToBuffer(Buff, cast(uint)In.length);
    if (In.length == 0) return;
    Buff ~= cast(ubyte[])In;
}

void WriteIntToBuffer(T)(ref ubyte[] Buff, T In) {
	pragma(inline);
    static if (T.sizeof == 1)
        WriteBinToBuffer(Buff, [ cast(ubyte)In ]);
    else if (is(T == bool))
        WriteBinToBuffer(Buff, [ cast(ubyte)(In ? 1 : 0) ]);
    else if (T.sizeof == 2) {
        if (cast(ushort)In < 255)
            WriteBinToBuffer(Buff, [ cast(ubyte)In ]);
        else {
            WriteBinToBuffer(Buff, [ cast(ubyte)255 ]);
            WriteBinToBuffer(Buff, [
                cast(ubyte)(In >> 8),
                cast(ubyte)(In & 0xFF) ]);
        }
    } else if (T.sizeof == 4) {
        if (cast(uint)In < 254)
            WriteBinToBuffer(Buff, [ cast(ubyte)In ]);
        else if (cast(uint)In < USHRT_MAX) {
            WriteBinToBuffer(Buff, [ cast(ubyte)254 ]);
            WriteBinToBuffer(Buff, [
                cast(ubyte)(In >> 8),
                cast(ubyte)(In & 0xFF) ]);
        } else {
            WriteBinToBuffer(Buff, [ cast(ubyte)255 ]);
            WriteBinToBuffer(Buff, [
                cast(ubyte)(In >> 24),
                cast(ubyte)((In >> 16) & 0xFF),
                cast(ubyte)((In >> 8) & 0xFF),
                cast(ubyte)(In & 0xFF) ]);
        }
    } else if (T.sizeof == 8) {
        if (cast(ulong)In < 253)
            WriteBinToBuffer(Buff, [ cast(ubyte)In ]);
        else if (cast(ulong)In < USHRT_MAX) {
            WriteBinToBuffer(Buff, [ cast(ubyte)253 ]);
            WriteBinToBuffer(Buff, [
                cast(ubyte)(In >> 8),
                cast(ubyte)(In & 0xFF) ]);
        } else if (cast(ulong)In < UINT_MAX) {
            WriteBinToBuffer(Buff, [ cast(ubyte)254 ]);
            WriteBinToBuffer(Buff, [
                cast(ubyte)(In >> 24),
                cast(ubyte)((In >> 16) & 0xFF),
                cast(ubyte)((In >> 8) & 0xFF),
                cast(ubyte)(In & 0xFF) ]);
        } else {
            WriteBinToBuffer(Buff, [ cast(ubyte)255 ]);
            WriteBinToBuffer(Buff, [
                cast(ubyte)(cast(ulong)In >> 56),
                cast(ubyte)((cast(ulong)In >> 48) & 0xFF),
                cast(ubyte)((cast(ulong)In >> 40) & 0xFF),
                cast(ubyte)((cast(ulong)In >> 32) & 0xFF),
                cast(ubyte)((In >> 24) & 0xFF),
                cast(ubyte)((In >> 16) & 0xFF),
                cast(ubyte)((In >> 8) & 0xFF),
                cast(ubyte)(In & 0xFF) ]);
        }
    }
}

T T2B(T)(ubyte[] In) { pragma(inline); In.length = T.sizeof; return *cast(T*)In; }

auto ReadInt(T)(Stream stream) {
	pragma(inline);
	
    ubyte[] data;
    data.length = 1;
    stream.read(data);

    auto first = data[0];

    static if (T.sizeof == 1)
        return cast(T)first;
    else if (is(T == bool))
        return first == 1;
    else if (T.sizeof == 2) {
        if (first == 255)
            return T2B!T(StreamReadTBytes!ushort(stream));
        else
            return cast(T)first;
    } else if (T.sizeof == 4) {
        if (first == 255)
            return T2B!T(StreamReadTBytes!uint(stream));
        else if (first == 254)
            return T2B!T(StreamReadTBytes!ushort(stream));
        else
            return cast(T)first;
    } else if (T.sizeof == 8) {
        if (first == 255)
            return T2B!T(StreamReadTBytes!ulong(stream));
        else if (first == 254)
            return T2B!T(StreamReadTBytes!uint(stream));
        else if (first == 253)
            return T2B!T(StreamReadTBytes!ushort(stream));
        else
            return cast(T)first;
    }
}

string ReadString(Stream stream) {
	pragma(inline);
	
    auto len = ReadInt!uint(stream);
    if (len == 0) return "";
    return cast(string)StreamRead(stream, cast(int)len);
}

void hexDump(ubyte[] data, uint mark_pos = 0)
{
import std.format;

    try {
        auto writer = appender!string();
        formattedWrite(writer, "Length: %d bytes", data.length);
        if(mark_pos) {
            if(mark_pos < data.length) {
                formattedWrite(writer, "; Mark Byte: %d, Line %d", mark_pos, mark_pos/16);
            } else {
                formattedWrite(writer, "; Mark: [out of range]");
            }
        } else {
            mark_pos = cast(uint)data.length;
        }
        formattedWrite(writer, "\n");

        //convert byte to hex
        char[2] hex;
        auto toHex = function (ubyte x, char[2] hex)
        {
            static const char[16] char_array = "0123456789abcdef";
            hex[0] = char_array[(x >> 4)];
            hex[1] = char_array[(x & 0xF)];
        };

        //print all full lines
        uint pos = 0;
        for(ushort line = 0; line < data.length/16; line++)
        {
            //print current position
            formattedWrite(writer, "%d", pos);
            formattedWrite(writer, "\t");

            for (ubyte i = 0; i < 8; i++, pos++) {
                formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
                ubyte c =  data[pos];
                //toHex(c, hex);
                formattedWrite(writer, "%02x", data[pos]);
            }
            formattedWrite(writer, " ");
            for (ubyte i = 0; i < 8; i++, pos++) {
                formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
                ubyte c =  data[pos];
                //toHex(c, hex);
                formattedWrite(writer, "%02x", data[pos]);
            }

            formattedWrite(writer, "  |");
            pos -= 16;

            for (ubyte i = 0; i < 8; i++, pos++) {
                char c = data[pos];
                formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
            }
            formattedWrite(writer, " ");
            for (ubyte i = 0; i < 8; i++, pos++) {
                char c = data[pos];
                formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
            }
            formattedWrite(writer, "|\n");
        }

        uint lpos = cast(uint)(data.length - pos);
        if(lpos == 0) return;

        formattedWrite(writer, "%d", pos);
        formattedWrite(writer, "\t");

        if(lpos < 9) //print lines with less then 9 hex values
        {
            for (ubyte i = 0; i < lpos; i++, pos++) {
                formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
                ubyte c =  data[pos];
                //toHex(c, hex);
                formattedWrite(writer, "%02x", data[pos]);
            }

            //padding for hex field
            for (ubyte i = 0; i < (3 * 16) - (3 * lpos) + 1; i++) {
                formattedWrite(writer, " ");
            }

            formattedWrite(writer, "  |");
            pos -= lpos;

            for (ubyte i = 0; i < lpos; i++, pos++) {
                char c = data[pos];
                formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
            }
            formattedWrite(writer, " ");

            //padding for text field
            for (ubyte i = 0; i < 16 - lpos; i++) {
                formattedWrite(writer, " ");
            }
            formattedWrite(writer, "|\n");
        }
        else //print lines with less then 16 hex values
        {
            for (ubyte i = 0; i < 8; i++, pos++) {
                formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
                ubyte c =  data[pos];
                //toHex(c, hex);
                formattedWrite(writer, "%02x", data[pos]);
            }
            formattedWrite(writer, " ");
            for (ubyte i = 0; i < lpos - 8; i++, pos++) {
                formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
                ubyte c =  data[pos];
                //toHex(c, hex);
                formattedWrite(writer, "%02x", data[pos]);
            }
            //padding for hex field
            for (ubyte i = 0; i < (3 * 16) - (3 * lpos); i++) {
                formattedWrite(writer, " ");
            }

            formattedWrite(writer, "  |");
            pos -= lpos;

            for (ubyte i = 0; i < 8; i++, pos++) {
                char c = data[pos];
                formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
            }
            formattedWrite(writer, " ");
            for (ubyte i = 0; i < lpos - 8; i++, pos++) {
                char c = data[pos];
                formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
            }

            //padding for text field
            for (ubyte i = 0; i < 16 - lpos; i++) {
                formattedWrite(writer, " ");
            }
            formattedWrite(writer, "|\n");
        }
        logInfo(writer.data);
    } catch (Exception ex) {
    }
}

void WriteBinToBuffer(ref ubyte[] Buff, ubyte[] In) {
	pragma(inline);
    if (sum(In) == 0) {
        // HACKHACK
        ubyte[] h;
        h.length = In.length;

        Buff ~= h;
        return;
    }

    version(LittleEndian)
        reverse(In);

    Buff ~= In;
}

void WriteBinToBuffer(T)(ref ubyte[] Buff, T In) {
	pragma(inline);
    Buff.length += In.sizeof;
    auto inBytes = nativeToBigEndian(In);
    memcpy(Buff.ptr + Buff.length - In.sizeof, inBytes.ptr, In.sizeof);

	debug logTrace("Writing RAW integer %d, type %s", In, fullyQualifiedName!(T));
}

ubyte[] StreamRead(Stream stream, uint Size) {
	pragma(inline);
    ubyte[] data;
    data.length = Size;
    stream.read(data);
    return data;
}

ubyte[] StreamReadTBytes(T)(Stream stream) {
	pragma(inline);
    auto ret = StreamRead(stream, T.sizeof);
    version (LittleEndian)
        reverse(ret);

    return ret;
}

auto StreamRead(T)(Stream stream) {
	pragma(inline);
    ubyte[] data;
    data.length = T.sizeof;
    stream.read(data);
    return data.read!(T, Endian.bigEndian)();
}
