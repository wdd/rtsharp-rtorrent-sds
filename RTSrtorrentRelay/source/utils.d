module utils;

import std.file;
import std.path;
import std.parallelism;

void copyParallel(in string pathname, in string destDir){
    DirEntry deSrc = DirEntry(pathname);
    string[] files;

    if (!exists(destDir)){
        mkdirRecurse (destDir); // makes dest root and all required parents
    }
 	DirEntry destDe = DirEntry(destDir);
    if(!destDe.isDir()){
        throw new FileException( destDe.name, " is not a directory");
    }
    string destName = destDe.name ~ '/';
    string destRoot = destName ~ baseName(deSrc.name);

    if(!deSrc.isDir()) {
        copy(deSrc.name,destRoot);
    } else {
        auto srcLen = deSrc.name.length;
        mkdir(destRoot);

        // make an array of the regular files only, also create the directory structure
        // Since it is SpanMode.breadth, can just use mkdir
 		foreach(DirEntry e; dirEntries(deSrc.name, SpanMode.breadth, false)){
            if (attrIsDir(e.linkAttributes)){
                mkdir(destRoot ~ e.name[srcLen..$]);
            }
            else{
                files ~= e.name;
            }
 		}

        // parallel foreach for regular files
        foreach(fn ; taskPool.parallel(files,100)) {
            string dfn = destRoot ~ fn[srcLen..$];
            copy(fn,dfn);
        }
    }
}

ulong folderSize(string path) {
	import std.algorithm.iteration : sum, map;
	return dirEntries(path, SpanMode.depth)
		.map!(x => x.isDir() ? folderSize(x.name) : x.size)
		.sum!();
}