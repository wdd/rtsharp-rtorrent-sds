module xpaths;

static class XPaths {
immutable:
static:
	string ALL_TORRENTS = "//methodResponse/params/param/value/array/data/value/array/data/value/*";
    string RT_SETTING_STRING = "//methodResponse/params/param/value/array/data/value/array/data/value/string";
	string RT_SETTINGS = "//methodResponse/params/param/value/array/data/value/array/data/value";
	string SINGLE_RESPONSE = "//methodResponse/params/param/value/string";
}
